#ifndef SCANNER_H 
#define SCANNER_H
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <malloc.h>
#include <stdlib.h>
#include <string>
#include "defs.h"
#include <cctype>
#include <unordered_map>

typedef struct Lexem{
	struct Position position;
	std::string lexem;
	std::string typestr;
	TokenType type;
} lexem;


class Lexer{
	public:
		lexem next();
		char getChar();
		void ungetChar(char ch);
		bool openFile(const std::string &filename);
		bool init();
		bool isLetter(char ch);
		bool isNumber(char ch);
		Lexer();
		Lexer(std::string text, std::string type);
		~Lexer();
	protected:
		void skip();
		char *file;
		int length;
		int pos;
		int line;
		int lexlen;
		int offset;

		std::string buffer;
		bool isInit;
		typedef	std::unordered_map<TokenType, const char *> TokenToStringMap;
		TokenToStringMap tokenMap;

		typedef	std::unordered_map<std::string, TokenType> StringToToken;
		StringToToken keywordMap;

};


#endif /* ifndef SCANNER_H */
