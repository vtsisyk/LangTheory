#ifndef STATES_H 
#define STATES_H
#define MAX_IDENTIFIER_SIZE 256

namespace Tokens{
	enum Type{

		UNKNOWN              = 0x0,
		IDENTIFIER           = 0x1,           /* идентификатор */
		IDENTIFIER_TOOLONG   = 0x2,
		CONST10              = 0x3,
		CONST10_TOOLONG      = 0x4,
		
		WHITESPACE        = 0x0100,
		COMMENT_MULTILINE = WHITESPACE | 0x1,
		COMMENT_EOL       = WHITESPACE | 0x2,

			/* арифметические операции */
		OPERATOR               = 0x200,
		OPERATOR_MATH          = OPERATOR      | 0x8,
		OPERATOR_MATH_PLUS     = OPERATOR_MATH | 0x1, /* + */
		OPERATOR_MATH_MINUS    = OPERATOR_MATH | 0x2, /* - */
		OPERATOR_MATH_DIVISION = OPERATOR_MATH | 0x3, /* / */
		OPERATOR_MATH_MULTIPLY = OPERATOR_MATH | 0x4, /* * */
		OPERATOR_MATH_MODULO   = OPERATOR_MATH | 0x5, /* % */
		OPERATOR_MATH_ASSIGN   = OPERATOR_MATH | 0x6, /* = */

		/* битовые операции */
		OPERATOR_BIT     = OPERATOR     | 0x400,
		OPERATOR_BIT_AND = OPERATOR_BIT | 0x1, /* & */
		OPERATOR_BIT_OR  = OPERATOR_BIT | 0x2, /* | */
		OPERATOR_BIT_NOT = OPERATOR_BIT | 0x3, /* ~ */
		OPERATOR_BIT_XOR = OPERATOR_BIT | 0x4, /* ^ */
		
		/* логические операции */
		OPERATOR_LOGIC     = OPERATOR       | 0x20,
		OPERATOR_LOGIC_AND = OPERATOR_LOGIC | 0x1, /* && */
		OPERATOR_LOGIC_OR  = OPERATOR_LOGIC | 0x2, /* || */
		OPERATOR_LOGIC_NOT = OPERATOR_LOGIC | 0x3, /* ! */
		OPERATOR_LOGIC_LT  = OPERATOR_LOGIC | 0x4, /* < */
		OPERATOR_LOGIC_GE  = OPERATOR_LOGIC | 0x5, /* > */
		OPERATOR_LOGIC_BEQ = OPERATOR_LOGIC | 0x6, /* >= */
		OPERATOR_LOGIC_LEQ = OPERATOR_LOGIC | 0x7, /* <= */
		OPERATOR_LOGIC_NEQ = OPERATOR_LOGIC | 0x8, /* != */
		OPERATOR_LOGIC_EQ  = OPERATOR_LOGIC | 0x9, /* == */
		
		/* language keywords 
		 * https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html
		 */
		KEYWORD              = 0x4000,
		KEYWORD_ABSTRACT     = KEYWORD | 0x1, /* abstract */
		KEYWORD_ASSERT       = KEYWORD | 0x2, /* assert */
		KEYWORD_BOOLEAN      = KEYWORD | 0x3, /* boolean */
		KEYWORD_BREAK        = KEYWORD | 0x4, /* break */
		KEYWORD_BYTE         = KEYWORD | 0x5, /* byte */
		KEYWORD_CASE         = KEYWORD | 0x6, /* case */
		KEYWORD_CATCH        = KEYWORD | 0x7, /* catch */
		KEYWORD_CHAR         = KEYWORD | 0x8, /* char */
		KEYWORD_CLASS        = KEYWORD | 0x9, /* class */
		KEYWORD_CONST        = KEYWORD | 0xa, /* const */
		KEYWORD_CONTINUE     = KEYWORD | 0xb, /* continue */
		KEYWORD_DEFAULT      = KEYWORD | 0xc, /* default */
		KEYWORD_DO           = KEYWORD | 0xd, /* do */
		KEYWORD_DOUBLE       = KEYWORD | 0xe, /* double */
		KEYWORD_ELSE         = KEYWORD | 0xf, /* else */
		KEYWORD_ENUM         = KEYWORD | 0x10, /* enum */
		KEYWORD_EXTENDS      = KEYWORD | 0x11, /* extends */
		KEYWORD_FINAL        = KEYWORD | 0x12, /*  final */
		KEYWORD_FINALLY      = KEYWORD | 0x13, /* finally */
		KEYWORD_FLOAT        = KEYWORD | 0x14, /* float */
		KEYWORD_FOR          = KEYWORD | 0x15, /* for */
		KEYWORD_GOTO         = KEYWORD | 0x16, /* goto, not used */
		KEYWORD_IF           = KEYWORD | 0x17, /* if */
		KEYWORD_IMPLEMENTS   = KEYWORD | 0x18, /* implements */
		KEYWORD_IMPORT       = KEYWORD | 0x19, /* import */
		KEYWORD_INSTANCEOF   = KEYWORD | 0x1a, /* instanceof */
		KEYWORD_INT          = KEYWORD | 0x1b, /* int */
		KEYWORD_INTERFACE    = KEYWORD | 0x1c, /* interface */
		KEYWORD_LONG         = KEYWORD | 0x1d, /* long */
		KEYWORD_NATIVE       = KEYWORD | 0x1e, /* native */
		KEYWORD_NEW          = KEYWORD | 0x1f, /* new */
		KEYWORD_PACKAGE      = KEYWORD | 0x20, /* package */
		KEYWORD_PRIVATE      = KEYWORD | 0x21, /* private */
		KEYWORD_PROTECTED    = KEYWORD | 0x22, /* protected */
		KEYWORD_PUBLIC       = KEYWORD | 0x23, /* public */
		KEYWORD_RETURN       = KEYWORD | 0x24, /* return */
		KEYWORD_SHORT        = KEYWORD | 0x25, /* short */
		KEYWORD_STATIC       = KEYWORD | 0x26, /* static */
		KEYWORD_STRICTFP     = KEYWORD | 0x27, /* strictfp */
		KEYWORD_SUPER        = KEYWORD | 0x28, /* super */
		KEYWORD_SWITCH       = KEYWORD | 0x29, /* switch */
		KEYWORD_SYNCHRONIZED = KEYWORD | 0x2a, /* synchronized */
		KEYWORD_THIS         = KEYWORD | 0x2b, /* this */
		KEYWORD_THROW        = KEYWORD | 0x2c, /* throw */
		KEYWORD_THROWS       = KEYWORD | 0x2d, /* throws */
		KEYWORD_TRANSIENT    = KEYWORD | 0x2e, /* transient */
		KEYWORD_TRY          = KEYWORD | 0x2f, /* try */
		KEYWORD_VOID         = KEYWORD | 0x30, /* void */
		KEYWORD_VOLATILE     = KEYWORD | 0x31, /* volatile */
		KEYWORD_WHILE        = KEYWORD | 0x32, /* while */

		/* скобки  */
		SEPARATOR            = 0x8000,
		SEPARATOR_LPAR       = SEPARATOR | 0x1, /* ( */
		SEPARATOR_RPAR       = SEPARATOR | 0x2, /* ) */
		SEPARATOR_LBRACE     = SEPARATOR | 0x3, /* { */
		SEPARATOR_RBRACE     = SEPARATOR | 0x4, /* } */
		SEPARATOR_LSBRACE    = SEPARATOR | 0x5, /* [ */
		SEPARATOR_RSBRACE    = SEPARATOR | 0x6, /* ] */
		/* разделители */
		SEPARATOR_DOT        = SEPARATOR | 0x7, /* . */
		SEPARATOR_SEMICOLON  = SEPARATOR | 0x8, /* ; */
		SEPARATOR_COLON      = SEPARATOR | 0x9, /* , */
		SEPARATOR_WHITESPACE = SEPARATOR | 0xa, /* ' ' */
		SEPARATOR_SQUOTE     = SEPARATOR | 0xb, /*  \' */
		SEPARATOR_DQUOTE     = SEPARATOR | 0xc, /* \" */

	
		OTHER        = 0x9000,
		OTHER_STRING = OTHER | 0x1,
		OTHER_AT     = OTHER | 0x2,
		END                    = 0xffff /* EOF */
	};
	
}
typedef Tokens::Type TokenType;

struct Position{
	int line;
	int offset;
};

#endif /* ifndef STATES_H */
