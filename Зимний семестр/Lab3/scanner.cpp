#include "lexer.h"

int main(int argc, const char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Usage: %s filename\n", argv[0]);
		return 1;
	}

	Lexer lex(argv[1]);
	lex.parseLexems();
}
