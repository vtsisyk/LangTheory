#ifndef SCANNER_H 
#define SCANNER_H
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <malloc.h>
#include <stdlib.h>
#include "defs.h"

/* это код на C/C++ */
class Lexer{
	public:
	Lexer(const char *filename);
	int checkkeyword(char *id);
	void print_info(int state, char *str, int line);
	int chartype(char c);
	int parseLexems();
	void doneHere();
	private:
		/* файл в памяти */
		char *file;
		/* текущая лексема */
		char lexem[MAX_LEX];
		/* файл на диске */
		FILE *in;
		/* Reference to an object of type char*, whose value is set by the function to the next character in str after the numerical value.
		 * This parameter can also be a null pointer, in which case it is not used.
		 */
		char *pEnd;
		/* текущий символ */
		char smbl;
		/* в нее считываю CONST10 и проверяю на то, не больше ли она int */
		long int noverflow;
		/* позиция в files */
		int  i;
		/* предыдущая позиция */
		int prev;
		/* длина files */
		int  length;
		/* длина лексемы */
		int  lexlen;
		/* что за символ считали */
		int  smbl_state;
		/* состояние автомата */
		int  state;
		/* количество строк */
		int  line;

};

#endif /* ifndef SCANNER_H */
