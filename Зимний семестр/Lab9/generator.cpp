#include "generator.h"


Generator::Generator(std::string filename)
{
	grammar = new SimpleGrammar(filename);
	grammar->parseGrammar();
	for(auto &a :grammar->nonTerminals)
		follow[a] = std::unordered_set<std::string>();
}

void Generator::buildTable()
{

	for(auto const &str : grammar->nonTerminals)
		first1(str);

	grammar2 = grammar->withoutEps();
	auto ttt = std::unordered_set<std::string>();
	follow1(grammar2.nonTerminals[0], "#", ttt);


	table = new Table(grammar->nonTerminals, grammar->terminals, grammar->nonTerminals[0]);
	for(auto &e : grammar->rulesMap){
		for(auto &rule : e.second){
			std::unordered_set<std::string> set= rule.first;
			if(set.size() == 1 && set.find("#") != set.end()){
					set = follow[e.first];
			}

			for(auto &terminal : set){
				table->add(e.first, terminal, rule.rule);
			}
		}
	}

	table->toCSV();
}

std::unordered_set<std::string> Generator::first1(std::string str)
{
	std::unordered_set<std::string> result;

	if(grammar->rulesMap.find(str) == grammar->rulesMap.end()) {
		result.insert(str);

	} else {
		for(auto &rule : grammar->rulesMap[str]){
			for (auto const &s : first1(rule.rule[0])) {
				if(s == "#" && rule.rule.size() > 1)
					for(auto const &rules : first1(rule.rule[1]))
					rule.first.insert(rules);
				else
					rule.first.insert(s);
			}
			for(auto const &rules : rule.first)
				result.insert(rules);
		}
	}
	return result;

}

void Generator::follow1(std::string a, std::string b, std::unordered_set<std::string> &used)
{
	
	if(used.size() > 0 && used.find(a + "$" + b) != used.end())
		return;
		
	used.insert(a + "$" + b);
	if(grammar2.rulesMap.find(b) == grammar2.rulesMap.end())
		follow[a].insert(b);

	for(auto &l : grammar2.rulesMap[a]){
		std::vector<std::string> list(l.rule);
		list.push_back(b);
		for(int i = 0; i < list.size() - 1; i++){
			if(grammar2.rulesMap.find(list[i]) != grammar2.rulesMap.end())
				follow1(list[i], list[i+1], used);
		}
	}

	if(grammar2.rulesMap.find(b) != grammar2.rulesMap.end()){
		for(auto &l : grammar2.rulesMap[b]){
			std::vector<std::string> list(l.rule);
			list.insert(list.begin(), a);
			for(int i = 0; i < list.size() - 1; i++){
				if(grammar2.rulesMap.find(list[i]) != grammar2.rulesMap.end())
					follow1(list[i], list[i+1], used);
			}

		}
	}

}

