#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cctype>
#include <string.h>
#include <unordered_map>
#include <unordered_set>

#include "defs.h"
#include "lexer.h"
class SimpleGrammar{


	public:
		struct Rule{
				std::unordered_set<std::string> first;
				std::vector<std::string> rule;
		};
		typedef	std::unordered_map<std::string, std::vector<Rule>> rules;
		rules rulesMap;
		std::vector<std::string> nonTerminals; 
		std::unordered_map<std::string, TokenType> terminals; 


		SimpleGrammar();

		SimpleGrammar(SimpleGrammar *g);
		SimpleGrammar(std::string);
		SimpleGrammar withoutEps();

		void removeEpsilon();
		void removeEpsilon(std::string a, bool full, 
				std::unordered_set<std::string> used);
		void parseGrammar();
	protected:
		std::ifstream file;
		std::vector<std::string> in;
		bool openFile(const std::string &filename);


		bool initiated;

		std::vector<std::string> split(const std::string &s, const char* delim);

		
		static inline void ltrim(std::string &s);
		static inline void rtrim(std::string &s);
		inline std::string trim(std::string s);
		TokenType getTerminalType(std::string str);
		
		};

