#include "table.h"

Table::Table(std::vector<std::string> nonTerminals, std::unordered_map<std::string, TokenType> terminals, std::string s)
{


	rows = nonTerminals.size();
	cols = terminals.size();
	cells = new Cell*[nonTerminals.size()];
	for(int i = 0 ; i < nonTerminals.size();i++){
		cells[i] = new Cell[terminals.size()];
	}

	for (auto &str : nonTerminals) {
		nonTerminalsMapping[str] = NonTerminal(str, nonTerminalsMapping.size());
	}

	axiom = nonTerminalsMapping[s];
	Terminal tmpEnd;

	for(auto & terminal : terminals){
//		std::cout << terminal.first << " +++ " << terminal.second << "\n";
		std::string name = terminal.first;
		terminalsMapping[name] = Terminal(name, terminalsMapping.size(), terminal.second);
		typesMapping[terminal.second] = typesMapping.size();

		if(terminal.second == TokenType::END)
			tmpEnd = terminalsMapping[name];
	}

	end = tmpEnd;

	std::vector<Element *> res;
	res.push_back(&terminalsMapping["#"]);
	cells[nonTerminalsMapping["условие_А"].getIndex()][terminalsMapping["}"].getIndex()].push_back(res);
}


void Table::add(std::string nonTerminal, std::string terminal, std::vector<std::string> rule)
{
	int a = nonTerminalsMapping[nonTerminal].getIndex();
	int b = terminalsMapping[terminal].getIndex();

	std::vector<Element *> result;

	std::vector<Element *> res;
	res.push_back(&terminalsMapping["#"]);
	for (int i = rule.size() - 1; i >= 0; i--) {
		std::string v = rule[i];
	/*	if(v == "#"){
			for(auto cc : terminalsMapping){
				cells[a][cc.second.getIndex()].push_back(res); 
			}
			continue;
		}
		*/
		if(nonTerminalsMapping.find(v) != nonTerminalsMapping.end())
			result.push_back(&nonTerminalsMapping[v]);
		else{
			result.push_back(&terminalsMapping[v]);
		}
	}
	cells[a][b].push_back(result);
}

void Table::toCSV()
{
	std::fstream file("table.csv", std::fstream::out);

	file << "\t";
	for(int i = 0; i < cols ; i++){
		for(auto &row : terminalsMapping){
			if(row.second.getIndex()  == i){
				file << "\"" + row.first + "\"" << "\t";
			}
		}
	}
	file<<"\n";

	for (int i = 0; i < rows; ++i) {
		for(auto &r : nonTerminalsMapping){
			if(r.second.getIndex()  == i)
				file << r.first << "\t";
		}
		for (int j = 0; j < cols; j++) {
			for(auto &rules : cells[i][j]){
				for(auto &rule: rules){
					file << rule->string() << " ";
				}
			}
				file << "\t";
		}
				file << "\n";
		
	}

}

Terminal *Table::getEnd() 
{
	return &end;
}
NonTerminal *Table::getAxiom()
{
	return &axiom;
}
