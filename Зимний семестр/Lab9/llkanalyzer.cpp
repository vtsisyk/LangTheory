#include "llkanalyzer.h"
LLkAnalyzer::LLkAnalyzer(std::string grammar, std::string prog)
{
	Generator gen = Generator(grammar);
	gen.buildTable();
	controlTable = gen.table;

	scanner = new Lexer(prog, "file");
}

/*
bool isTerminal(Element *tocheck)
{
	return dynamic_cast<const Terminal *>(tocheck) != nullptr;
}
*/
template<class T, class K>
inline bool isType(const K &k) {
	    return dynamic_cast<const T *>(k) != nullptr;
}
void LLkAnalyzer::analyze()
{
	std::stack<Element *> stack;

	stack.push(controlTable->getEnd());
	stack.push(controlTable->getAxiom());

	lexem lex = scanner->next();
	static Table::Cell cell;
	while(true) {
	
		Element *current = stack.top();
		stack.pop();

		if(lex.type == TokenType::END && stack.top()->getValue() == "#"){
			std::cout<< "\nIt's Good\n";
			break;
		}
		
		std::cout << current->getValue() <<" --- " << lex.lexem << "\n";
		
		if (isType<Terminal>(current)){
			Terminal *terminal = (Terminal *) current;
			if(lex.type == Tokens::END){
				std::cout << "Unexpected End\n";
				break;
			}

			if(current->getValue() == "#" ){
				continue;
			}


			if(lex.type != terminal->getType()) {

				std::string msg1, msg2;
				msg1 =  "expected " + scanner->getTypeName(terminal->getType());
				msg2 =  "but found " + lex.typestr;
				Errprint(scanner, msg1,msg2,lex);
				break;
			}

			lex = scanner->next();
			std::cout << "  READED: " << "\"" + lex.lexem + "\"" << "\n";
			
		} else {

			NonTerminal * nonTerminal = (NonTerminal *) current;

			int row,col;

			row = controlTable->nonTerminalsMapping[current->getValue()].getIndex();

				for(auto &i: controlTable->terminalsMapping){
				if(lex.type == i.second.getType()){
					col = i.second.getIndex();
					break;
				}
			}

			 cell = controlTable->cells[row][col];

			if(cell.empty()) {
				//std::cout << Tokens::OPERATOR_LOGIC_LT << " --- " << Tokens::OPERATOR_LOGIC_LEQ << "\n";

				stack.pop();
				stack.pop();
				std::string msg1, msg2;
				if(lex.type == Tokens::END){
				
					msg1 = "Unexpected end of file";
					msg2 = "while analyzing "+ nonTerminal->getValue();
				} else {
					msg1 = "wrong character \'" + lex.lexem + "\'";
					msg2 = "when analyzing " +  nonTerminal->getValue();
				}
				Errprint(scanner, msg1,msg2,lex);
				/* тут ошибка */
				break;
			}
			for (auto &i : cell[0]) {
				stack.push(i);
			}
		}
	}
}

