#include "grammar.h"


/* TODO: regexp support */
std::vector<std::string> SimpleGrammar::split(const std::string &s, const char* delim){
	std::vector<std::string> result;
    char *dup = strdup(s.c_str());
    char *token = strtok(dup, delim);
    while(token != NULL){
		std::string tmp = std::string(token);
		if(!tmp.empty())
			result.push_back(tmp);
        token = strtok(NULL, delim);
    }
    free(dup);
    return result;
}

bool SimpleGrammar::openFile(const std::string &filename)
{

	file.open(filename, std::ifstream::in);
	if(file.fail())
		return false;

	// если файл открылся, то мы его сразу построчно и считываем
	std::string line;
	while(std::getline(file, line)){
		if(!line.empty())
			in.push_back(line);
	}

	return true;
}
inline void SimpleGrammar::ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
		return !std::isspace(ch);
	}));
}

// trim from end (in place)
inline void SimpleGrammar::rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		return !std::isspace(ch);
	}).base(), s.end());
}

// trim from both ends (in place)
inline std::string SimpleGrammar::trim(std::string s) {
	ltrim(s);
	rtrim(s);
	return s;
}

void SimpleGrammar::parseGrammar()
{
	for(auto const& it : in){
		/*strtok uses symbols from string indepentently */
		std::vector<std::string> data = split(it, ":");
		std::string nt = trim(data[0]);
		std::vector<Rule> list;
		nonTerminals.push_back(nt);
		std::vector<std::string> sr = split(trim(data[1]), "|");
		for(auto const &rules : sr){
			Rule rule;
			/* only whitespace */
			std::vector<std::string> so = split(trim(rules), " ");
			for(auto const &sym : so){
				rule.rule.push_back(trim(sym));
			}
			list.push_back(rule);
		}
		rulesMap[nt] = list;
	}

	std::vector<std::string> terminalsSet; 
	for(auto const& rules : rulesMap){
		for (auto const &rule : rules.second) {
			for (auto const &t : rule.rule) {
				if(rulesMap.find(t) == rulesMap.end())
					terminalsSet.push_back(t);
			}
			
		}		
	}

	for (auto const &t : terminalsSet) {
		terminals[t] = getTerminalType(t);
	}


}

TokenType SimpleGrammar::getTerminalType(std::string str)
{
	if(str == "#")
		return Tokens::END;
	else if(str == "константа10")
		return Tokens::CONST10;
	else {
		return Lexer(str, "text").next().type;
	}
}
SimpleGrammar::SimpleGrammar(std::string file)
{
	// TODO: check if file wasn't opened
	initiated = openFile(file);
}

SimpleGrammar::SimpleGrammar()
{

}

SimpleGrammar::SimpleGrammar(SimpleGrammar *g)
{
	rulesMap = g->rulesMap;
	terminals = g->terminals;
	nonTerminals = g->nonTerminals;
}

SimpleGrammar SimpleGrammar::withoutEps()
{
	SimpleGrammar grammar(this);
	grammar.removeEpsilon();
	return grammar;

}

void SimpleGrammar::removeEpsilon()
{
	removeEpsilon("#", true, std::unordered_set<std::string>());
	terminals.erase(terminals.find("#"));
}

void SimpleGrammar::removeEpsilon(std::string a, bool full, std::unordered_set<std::string> used)
{
	used.insert(a);
	for (auto &e : rulesMap) {
		if(used.find(e.first) != used.end())
			continue;
		std::vector<Rule> add;
		std::vector<Rule>::iterator i;
		for(i = e.second.begin(); i < e.second.end(); i++){
			Rule rule = *(i);
			if(rule.rule.size() == 1 && rule.rule[0] == a){
				if(full)
					e.second.erase(i);
				removeEpsilon(e.first, e.second.size() == 0, used);
			} else {
				bool contains = false;
				for(auto &r : rule.rule)
					if(r == a)
						contains = true;
				if(contains){
					Rule nw;
					for(auto &s : rule.rule)
						if(s != a)
							nw.rule.push_back(s);
					add.push_back(nw);
				}
			}	
		}
		for (auto &ad : add) {
			e.second.push_back(ad);
		}
	}
}
