#ifndef ERRPRINT_H
#define ERRPRINT_H
#include "lexer.h"
#include <iostream>
#include <iomanip>
class Errprint{

	public:
		Errprint(Lexer *scanner, std::string msg1, std::string msg2, lexem lex);
};
#endif /* ifndef ERRPRINT_H */
