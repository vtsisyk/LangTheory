#ifndef LLKANALYZER_H
#define LLKANALYZER_H
#include "table.h"
#include "generator.h"
#include "errprint.h"
#include "lexer.h"
#include <stack>
#include <typeinfo>
class LLkAnalyzer{
	public:
		LLkAnalyzer(std::string, std::string);
		void analyze();
	protected: 
		Table *controlTable;
		Lexer *scanner;

};

#endif /* ifndef LLKANALYZER_H */
