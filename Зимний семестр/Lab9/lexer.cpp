#include "defs.h"
#include "lexer.h"
bool Lexer::openFile(const std::string &filename)
{

	FILE *instream;
	fname = filename;

	instream = fopen(filename.c_str(), "rb");
	if(instream == NULL){
		return false;
	}

	fseek(instream, 0, SEEK_END);
	length = ftell(instream);
	fseek(instream, 0, SEEK_SET);
	file = new char[length + 1];
	fread(file, length, 1, instream);
	fclose(instream);
	return true;
}

Lexer::Lexer()
{ 
	buffer.reserve(MAX_IDENTIFIER_SIZE);
	buffer.clear();
	line = 1;
	pos = 0;
	lexlen = 0;
	offset = 1;
	isInit = false;
	init();
}

Lexer::Lexer(std::string text, std::string type)
{ 
	buffer.reserve(MAX_IDENTIFIER_SIZE);
	buffer.clear();
	line = 1;
	pos = 0;
	lexlen = 0;
	offset = 1;
	isInit = false;

	length = text.length() + 1;
	if(type == "text"){
		file = new char[text.length() + 1];
		strcpy(file, text.c_str());
	} else if(type == "file"){
		openFile(text);
	}
	init();
}
Lexer::~Lexer()
{

}

bool Lexer::isLetter(char ch)
{
	if(std::isalpha(ch) || ch == '_')
		return true;
	else
		return false;
}
bool Lexer::isNumber(char ch)
{
	if(std::isdigit(ch))
		return true;
	return false;
}
bool Lexer::init()
{ 

	if(isInit)
		return true;
	keywordMap["abstract"]     = Tokens::KEYWORD_ABSTRACT;
	keywordMap["assert"]       = Tokens::KEYWORD_ASSERT;
	keywordMap["boolean"]      = Tokens::KEYWORD_BOOLEAN;
	keywordMap["break"]        = Tokens::KEYWORD_BREAK;
	keywordMap["byte"]         = Tokens::KEYWORD_BYTE;
	keywordMap["case"]         = Tokens::KEYWORD_CASE;
	keywordMap["catch"]        = Tokens::KEYWORD_CATCH;
	keywordMap["char"]         = Tokens::KEYWORD_CHAR;
	keywordMap["class"]        = Tokens::KEYWORD_CLASS;
	keywordMap["const"]        = Tokens::KEYWORD_CONST;
	keywordMap["continue"]     = Tokens::KEYWORD_CONTINUE;
	keywordMap["default"]      = Tokens::KEYWORD_DEFAULT;
	keywordMap["do"]           = Tokens::KEYWORD_DO;
	keywordMap["double"]       = Tokens::KEYWORD_DOUBLE;
	keywordMap["else"]         = Tokens::KEYWORD_ELSE;
	keywordMap["enum"]         = Tokens::KEYWORD_ENUM;
	keywordMap["extends"]      = Tokens::KEYWORD_EXTENDS;
	keywordMap["final"]        = Tokens::KEYWORD_FINAL;
	keywordMap["finally"]      = Tokens::KEYWORD_FINALLY;
	keywordMap["float"]        = Tokens::KEYWORD_FLOAT;
	keywordMap["for"]          = Tokens::KEYWORD_FOR;
	keywordMap["goto"]         = Tokens::KEYWORD_GOTO;
	keywordMap["if"]           = Tokens::KEYWORD_IF;
	keywordMap["implements"]   = Tokens::KEYWORD_IMPLEMENTS;
	keywordMap["import"]       = Tokens::KEYWORD_IMPORT;
	keywordMap["instanceof"]   = Tokens::KEYWORD_INSTANCEOF;
	keywordMap["int"]          = Tokens::KEYWORD_INT;
	keywordMap["interface"]    = Tokens::KEYWORD_INTERFACE;
	keywordMap["long"]         = Tokens::KEYWORD_LONG;
	keywordMap["native"]       = Tokens::KEYWORD_NATIVE;
	keywordMap["new"]          = Tokens::KEYWORD_NEW;
	keywordMap["package"]      = Tokens::KEYWORD_PACKAGE;
	keywordMap["private"]      = Tokens::KEYWORD_PRIVATE;
	keywordMap["protected"]    = Tokens::KEYWORD_PROTECTED;
	keywordMap["public"]       = Tokens::KEYWORD_PUBLIC;
	keywordMap["return"]       = Tokens::KEYWORD_RETURN;
	keywordMap["short"]        = Tokens::KEYWORD_SHORT;
	keywordMap["static"]       = Tokens::KEYWORD_STATIC;
	keywordMap["strictfp"]     = Tokens::KEYWORD_STRICTFP;
	keywordMap["super"]        = Tokens::KEYWORD_SUPER;
	keywordMap["switch"]       = Tokens::KEYWORD_SWITCH;
	keywordMap["synchronized"] = Tokens::KEYWORD_SYNCHRONIZED;
	keywordMap["this"]         = Tokens::KEYWORD_THIS;
	keywordMap["throw"]        = Tokens::KEYWORD_THROW;
	keywordMap["throws"]       = Tokens::KEYWORD_THROWS;
	keywordMap["transient"]    = Tokens::KEYWORD_TRANSIENT;
	keywordMap["try"]          = Tokens::KEYWORD_TRY;
	keywordMap["void"]         = Tokens::KEYWORD_VOID;
	keywordMap["volatile"]     = Tokens::KEYWORD_VOLATILE ;
	keywordMap["while"]        = Tokens::KEYWORD_WHILE;

	keywordMap["+"]  = Tokens::OPERATOR_MATH_PLUS;
	keywordMap["-"]  = Tokens::OPERATOR_MATH_MINUS;
	keywordMap["/"]  = Tokens::OPERATOR_MATH_DIVISION;
	keywordMap["*"]  = Tokens::OPERATOR_MATH_MULTIPLY;
	keywordMap["%"]  = Tokens::OPERATOR_MATH_MODULO;
	keywordMap["="]  = Tokens::OPERATOR_MATH_ASSIGN;
	keywordMap["&"]  = Tokens::OPERATOR_BIT_AND;
	keywordMap["|"]  = Tokens::OPERATOR_BIT_OR;
	keywordMap["~"]  = Tokens::OPERATOR_BIT_NOT;
	keywordMap["^"]  = Tokens::OPERATOR_BIT_XOR;
	keywordMap["&&"] = Tokens::OPERATOR_LOGIC_AND;
	keywordMap["||"] = Tokens::OPERATOR_LOGIC_OR;
	keywordMap["!"]  = Tokens::OPERATOR_LOGIC_NOT;
	keywordMap["<"]  = Tokens::OPERATOR_LOGIC_LT;
	keywordMap[">"]  = Tokens::OPERATOR_LOGIC_GE;
	keywordMap[">="] = Tokens::OPERATOR_LOGIC_BEQ;
	keywordMap["<="] = Tokens::OPERATOR_LOGIC_LEQ;
	keywordMap["!="] = Tokens::OPERATOR_LOGIC_NEQ;
	keywordMap["=="] = Tokens::OPERATOR_LOGIC_EQ;
	keywordMap["("]  = Tokens::SEPARATOR_LPAR;
	keywordMap[")"]  = Tokens::SEPARATOR_RPAR;
	keywordMap["{"]  = Tokens::SEPARATOR_LBRACE;
	keywordMap["}"]  = Tokens::SEPARATOR_RBRACE;
	keywordMap["["]  = Tokens::SEPARATOR_LSBRACE;
	keywordMap["]"]  = Tokens::SEPARATOR_RSBRACE;
	keywordMap["."]  = Tokens::SEPARATOR_DOT;
	keywordMap[";"]  = Tokens::SEPARATOR_SEMICOLON;
	keywordMap[","]  = Tokens::SEPARATOR_COLON;
	keywordMap["\'"] = Tokens::SEPARATOR_SQUOTE;
	keywordMap["\""] = Tokens::SEPARATOR_DQUOTE;
	keywordMap["@"]  = Tokens::OTHER_AT;
		
	tokenMap[Tokens::UNKNOWN]                = "UNKNOWN";
	tokenMap[Tokens::IDENTIFIER]             = "IDENTIFIER";
	tokenMap[Tokens::IDENTIFIER_TOOLONG]     = "IDENTIFIER_TOOLONG";
	tokenMap[Tokens::CONST10]                = "CONST10";
	tokenMap[Tokens::CONST10_TOOLONG]        = "CONST10_TOOLONG";
	tokenMap[Tokens::WHITESPACE]             = "WHITESPACE";
	tokenMap[Tokens::COMMENT_MULTILINE]      = "COMMENT_MULTILINE";
	tokenMap[Tokens::COMMENT_EOL]            = "COMMENT_EOL";
	tokenMap[Tokens::OPERATOR]               = "OPERATOR";
	tokenMap[Tokens::OPERATOR_MATH]          = "OPERATOR_MATH";
	tokenMap[Tokens::OPERATOR_MATH_PLUS]     = "OPERATOR_MATH_PLUS";
	tokenMap[Tokens::OPERATOR_MATH_MINUS]    = "OPERATOR_MATH_MINUS";
	tokenMap[Tokens::OPERATOR_MATH_DIVISION] = "OPERATOR_MATH_DIVISION";
	tokenMap[Tokens::OPERATOR_MATH_MULTIPLY] = "OPERATOR_MATH_MULTIPLY";
	tokenMap[Tokens::OPERATOR_MATH_MODULO]   = "OPERATOR_MATH_MODULO";
	tokenMap[Tokens::OPERATOR_MATH_ASSIGN]   = "OPERATOR_MATH_ASSIGN";
	tokenMap[Tokens::OPERATOR_BIT]           = "OPERATOR_BIT";
	tokenMap[Tokens::OPERATOR_BIT_AND]       = "OPERATOR_BIT_AND";
	tokenMap[Tokens::OPERATOR_BIT_OR]        = "OPERATOR_BIT_OR";
	tokenMap[Tokens::OPERATOR_BIT_NOT]       = "OPERATOR_BIT_NOT";
	tokenMap[Tokens::OPERATOR_BIT_XOR]       = "OPERATOR_BIT_XOR";
	tokenMap[Tokens::OPERATOR_LOGIC]         = "OPERATOR_LOGIC";
	tokenMap[Tokens::OPERATOR_LOGIC_AND]     = "OPERATOR_LOGIC_AND";
	tokenMap[Tokens::OPERATOR_LOGIC_OR]      = "OPERATOR_LOGIC_OR";
	tokenMap[Tokens::OPERATOR_LOGIC_NOT]     = "OPERATOR_LOGIC_NOT";
	tokenMap[Tokens::OPERATOR_LOGIC_LT]      = "OPERATOR_LOGIC_LT";
	tokenMap[Tokens::OPERATOR_LOGIC_GE]      = "OPERATOR_LOGIC_GE";
	tokenMap[Tokens::OPERATOR_LOGIC_BEQ]     = "OPERATOR_LOGIC_BEQ";
	tokenMap[Tokens::OPERATOR_LOGIC_LEQ]     = "OPERATOR_LOGIC_LEQ";
	tokenMap[Tokens::OPERATOR_LOGIC_NEQ]     = "OPERATOR_LOGIC_NEQ";
	tokenMap[Tokens::OPERATOR_LOGIC_EQ]      = "OPERATOR_LOGIC_EQ";
	tokenMap[Tokens::KEYWORD]                = "KEYWORD";
	tokenMap[Tokens::KEYWORD_ABSTRACT]       = "KEYWORD_ABSTRACT";
	tokenMap[Tokens::KEYWORD_ASSERT]         = "KEYWORD_ASSERT";
	tokenMap[Tokens::KEYWORD_BOOLEAN]        = "KEYWORD_BOOLEAN";
	tokenMap[Tokens::KEYWORD_BREAK]          = "KEYWORD_BREAK";
	tokenMap[Tokens::KEYWORD_BYTE]           = "KEYWORD_BYTE";
	tokenMap[Tokens::KEYWORD_CASE]           = "KEYWORD_CASE";
	tokenMap[Tokens::KEYWORD_CATCH]          = "KEYWORD_CATCH";
	tokenMap[Tokens::KEYWORD_CHAR]           = "KEYWORD_CHAR";
	tokenMap[Tokens::KEYWORD_CLASS]          = "KEYWORD_CLASS";
	tokenMap[Tokens::KEYWORD_CONST]          = "KEYWORD_CONST";
	tokenMap[Tokens::KEYWORD_CONTINUE]       = "KEYWORD_CONTINUE";
	tokenMap[Tokens::KEYWORD_DEFAULT]        = "KEYWORD_DEFAULT";
	tokenMap[Tokens::KEYWORD_DO]             = "KEYWORD_DO";
	tokenMap[Tokens::KEYWORD_DOUBLE]         = "KEYWORD_DOUBLE";
	tokenMap[Tokens::KEYWORD_ELSE]           = "KEYWORD_ELSE";
	tokenMap[Tokens::KEYWORD_ENUM]           = "KEYWORD_ENUM";
	tokenMap[Tokens::KEYWORD_EXTENDS]        = "KEYWORD_EXTENDS";
	tokenMap[Tokens::KEYWORD_FINAL]          = "KEYWORD_FINAL";
	tokenMap[Tokens::KEYWORD_FINALLY]        = "KEYWORD_FINALLY";
	tokenMap[Tokens::KEYWORD_FLOAT]          = "KEYWORD_FLOAT";
	tokenMap[Tokens::KEYWORD_FOR]            = "KEYWORD_FOR";
	tokenMap[Tokens::KEYWORD_GOTO]           = "KEYWORD_GOTO";
	tokenMap[Tokens::KEYWORD_IF]             = "KEYWORD_IF";
	tokenMap[Tokens::KEYWORD_IMPLEMENTS]     = "KEYWORD_IMPLEMENTS";
	tokenMap[Tokens::KEYWORD_IMPORT]         = "KEYWORD_IMPORT";
	tokenMap[Tokens::KEYWORD_INSTANCEOF]     = "KEYWORD_INSTANCEOF";
	tokenMap[Tokens::KEYWORD_INT]            = "KEYWORD_INT";
	tokenMap[Tokens::KEYWORD_INTERFACE]      = "KEYWORD_INTERFACE";
	tokenMap[Tokens::KEYWORD_LONG]           = "KEYWORD_LONG";
	tokenMap[Tokens::KEYWORD_NATIVE]         = "KEYWORD_NATIVE";
	tokenMap[Tokens::KEYWORD_NEW]            = "KEYWORD_NEW";
	tokenMap[Tokens::KEYWORD_PACKAGE]        = "KEYWORD_PACKAGE";
	tokenMap[Tokens::KEYWORD_PRIVATE]        = "KEYWORD_PRIVATE";
	tokenMap[Tokens::KEYWORD_PROTECTED]      = "KEYWORD_PROTECTED";
	tokenMap[Tokens::KEYWORD_PUBLIC]         = "KEYWORD_PUBLIC";
	tokenMap[Tokens::KEYWORD_RETURN]         = "KEYWORD_RETURN";
	tokenMap[Tokens::KEYWORD_SHORT]          = "KEYWORD_SHORT";
	tokenMap[Tokens::KEYWORD_STATIC]         = "KEYWORD_STATIC";
	tokenMap[Tokens::KEYWORD_STRICTFP]       = "KEYWORD_STRICTFP";
	tokenMap[Tokens::KEYWORD_SUPER]          = "KEYWORD_SUPER";
	tokenMap[Tokens::KEYWORD_SWITCH]         = "KEYWORD_SWITCH";
	tokenMap[Tokens::KEYWORD_SYNCHRONIZED]   = "KEYWORD_SYNCHRONIZED";
	tokenMap[Tokens::KEYWORD_THIS]           = "KEYWORD_THIS";
	tokenMap[Tokens::KEYWORD_THROW]          = "KEYWORD_THROW";
	tokenMap[Tokens::KEYWORD_THROWS]         = "KEYWORD_THROWS";
	tokenMap[Tokens::KEYWORD_TRANSIENT]      = "KEYWORD_TRANSIENT";
	tokenMap[Tokens::KEYWORD_TRY]            = "KEYWORD_TRY";
	tokenMap[Tokens::KEYWORD_VOID]           = "KEYWORD_VOID";
	tokenMap[Tokens::KEYWORD_VOLATILE]       = "KEYWORD_VOLATILE";
	tokenMap[Tokens::KEYWORD_WHILE]          = "KEYWORD_WHILE";
	tokenMap[Tokens::SEPARATOR]              = "SEPARATOR";
	tokenMap[Tokens::SEPARATOR_LPAR]         = "SEPARATOR_LPAR";
	tokenMap[Tokens::SEPARATOR_RPAR]         = "SEPARATOR_RPAR";
	tokenMap[Tokens::SEPARATOR_LBRACE]       = "SEPARATOR_LBRACE";
	tokenMap[Tokens::SEPARATOR_RBRACE]       = "SEPARATOR_RBRACE";
	tokenMap[Tokens::SEPARATOR_LSBRACE]      = "SEPARATOR_LSBRACE";
	tokenMap[Tokens::SEPARATOR_RSBRACE]      = "SEPARATOR_RSBRACE";

	tokenMap[Tokens::SEPARATOR_DOT]          = "SEPARATOR_DOT";
	tokenMap[Tokens::SEPARATOR_SEMICOLON]    = "SEPARATOR_SEMICOLON";
	tokenMap[Tokens::SEPARATOR_COLON]        = "SEPARATOR_COLON";
	tokenMap[Tokens::SEPARATOR_WHITESPACE]   = "SEPARATOR_WHITESPACE";
	tokenMap[Tokens::SEPARATOR_SQUOTE]       = "SEPARATOR_SQUOTE";
	tokenMap[Tokens::SEPARATOR_DQUOTE]       = "SEPARATOR_DQUOTE";
	tokenMap[Tokens::OTHER]                  = "OTHER";
	tokenMap[Tokens::OTHER_STRING]           = "OTHER_STRING";
	tokenMap[Tokens::OTHER_AT]               = "OTHER_AT";
	tokenMap[Tokens::END]                    = "END";
	isInit = true;
	return isInit;
} 

lexem Lexer::next()
{
	skip();
	lexem lex;

	int ch = file[pos];
	if(pos == length){
		lex.type = Tokens::END;
		return lex;
	}
	if(isNumber(ch)){
		do{
			buffer +=  ch;
			pos++;
			ch = file[pos];
		}while(isNumber(ch));
		lex.type = Tokens::CONST10;
		// TODO: Add regular checker
		if(buffer.length() > 20)
			lex.type = Tokens::CONST10_TOOLONG;
	}
	else if(isLetter(ch)){
		do{
			buffer +=  ch;
			pos++;
			ch = file[pos];
		} while(isLetter(ch) || isNumber(ch));

		StringToToken::const_iterator got = keywordMap.find(buffer);
		if(got == keywordMap.end()){
			lex.type = Tokens::IDENTIFIER;
			if(buffer.length() > MAX_IDENTIFIER_SIZE)
				lex.type = Tokens::IDENTIFIER_TOOLONG;
		}else{
			lex.type = got->second;
		}
	} else if((ch == '&' || ch == '|' || ch == '>' || ch == '<' ||
			  ch == '=' || ch == '!') && pos + 1 < length){
		buffer +=  ch;
		buffer += file[pos + 1];
		StringToToken::const_iterator got = keywordMap.find(buffer);
		if(got == keywordMap.end()){
			buffer.clear();
			buffer += ch;
			lex.type = keywordMap[buffer];
			pos+=1;
		} else {
			lex.type = got->second;
			pos+=2;
		}
	}else if(ch == '\"'){
		do{
			buffer +=  ch;
			pos++;
			ch = file[pos];
		}while(ch != '\"' || file[pos - 1] == '\\');
			buffer +=  ch;
			pos++;
			lex.type = Tokens::OTHER_STRING;
	} else {
		buffer +=  ch;
		StringToToken::const_iterator got = keywordMap.find(buffer);
		if(got == keywordMap.end()){
			lex.type = Tokens::UNKNOWN;
		}else{
			lex.type = got->second;
		}
			pos++;
	}
	lex.position.line = line;
	lex.typestr = tokenMap[lex.type];
	lex.lexem = buffer;
	lex.position.offset = offset;
	offset += buffer.length();
	buffer.clear();

	return lex;
}
void Lexer::skip()
{
	while(pos < length){
		/*
		 * UNIX systems use \n (even Mac, at least new ones)
		 * Windows use \r\n
		 */
		if(file[pos] == '\n'){
			line++;
			pos++;
			offset = 1;
		}
		else if(file[pos] == '\r' && file[pos + 1] == '\n'){
			line++;
			pos +=2;
			offset = 1;
		} else if(file[pos] == ' '){
			offset += 1;
			pos++;
		}else if(file[pos] == '\t'){
			offset += 4;
			pos++;
		}else if(file[pos] == '/' && file[pos + 1] == '/'){ 
			while(file[pos++] != '\n' && pos < length);
			line++;
			offset = 1;
		} else if(file[pos] == '/' && file[pos + 1] == '*'){
			pos += 2;
			offset +=2;
			while(!(file[pos] == '*' || file[pos + 1] == '/') && pos + 1 < length){
				if(file[pos] == '\n'){
					line++;
					offset = 1;
				}
			pos++;
			}
			pos+=2;
			offset +=2;
		} else {
			break;
		}
	}
}

std::string Lexer::getStrAt()
{

	int i = pos - 1;
	while(file[i] != '\n' && i >= 0){
		i--;
	}
	i++;
	std::string res = "";

	while (file[i] != '\n' ) {
		if(file[i] == '\t')
			res += "    ";
		else
			res += file[i];
		i++;
	}

	return res;
}
