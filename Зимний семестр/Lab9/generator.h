#ifndef GENERATOR_H
#define GENERATOR_H
#include "grammar.h"
#include "table.h"
#include <string>
#include <unordered_set>
class Generator{

	public:
		Generator(std::string);
		void buildTable();
		std::unordered_set<std::string> first1(std::string str);
		void follow1(std::string a, std::string b, std::unordered_set<std::string> &used);
		std::unordered_map<std::string, std::unordered_set<std::string>> follow;
		SimpleGrammar *grammar, grammar2;
		Table *table;
	private:


};
#endif
