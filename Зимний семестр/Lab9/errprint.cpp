#include "errprint.h"

Errprint::Errprint(Lexer *scanner, std::string msg1, std::string msg2, lexem lex)
{
	std::cout<< scanner->getFilename() << ":";
	std::cout<< lex.position.line << ":";
	std::cout<< lex.position.offset<< ": ";
	std::cout<< "\033[1;31merror: \033[0m";
	std::cout<< msg1;
	std::cout<< " ";
	std::cout<< msg2;
	std::cout<< "\n";
	if(lex.type != Tokens::END){
		std::cout<< scanner->getStrAt();
		std::cout<< "\n";
		std::cout<< std::setw(lex.position.offset + 1) << "^\n";
	}
}
