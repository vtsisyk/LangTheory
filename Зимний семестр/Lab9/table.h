#ifndef TABLE_H
#define TABLE_H
#include <string>
#include "defs.h"
#include <iostream>
#include <vector>
#include <unordered_map>
#include <fstream>
class Element{

	protected:
		std::string value;
		int index;

	public:
		int getIndex(){return index;}
		std::string  getValue(){return value;}
		Element(){}
		Element(std::string s, int i){
			value = s;
			index = i;
		}

		virtual std::string classType() {}
		std::string string(){
			return value;
		}
};


class Terminal : public Element{

	protected:
		TokenType type;
	public:
		Terminal(){}
		std::string classType() {
			return "Terminal";
		}

		Terminal(Element t){
			value = t.getValue();
			index = t.getIndex();
		}

		TokenType getType() {return type;}
		Terminal(std::string s, int i, TokenType t) : Element(s, i) {
			type = t;
		}

};

class NonTerminal : public Element{

	public:
		NonTerminal(){}
		
		NonTerminal(Element t){
			value = t.getValue();
			index = t.getIndex();
		}
		~NonTerminal(){}
		NonTerminal(std::string s, int i) : Element(s, i){}
};


class Table
{
	public:

	typedef std::vector<std::vector<Element *>> Cell;
	std::unordered_map<std::string, NonTerminal> nonTerminalsMapping;
	std::unordered_map<std::string, Terminal> terminalsMapping;
	std::unordered_map<TokenType, int> typesMapping;

	Cell **cells;

	int rows, cols;
	Terminal end;
	NonTerminal axiom;
public:
	Terminal *getEnd();
	NonTerminal *getAxiom();
	Table(std::vector<std::string> nonTerminals, std::unordered_map<std::string, TokenType> terminals, std::string s);
	void add(std::string nonTerminal, std::string terminal, std::vector<std::string> rule);

	void toCSV();
};
#endif /* TABLE_H */
