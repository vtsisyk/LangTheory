#include "defs.h"
#include "lexer.h"
Lexer::Lexer()
{
	/* в исходное состояние */
	i = last = line = 0;

	partly = false;
	prev = -1;

	doneHere();

}


Lexer::~Lexer()
{
	free(file);
}

int Lexer::openFile(const char *filename)
{

	/* открыл файл и считал побайтово 
	 * Длина всего файла - количество байт в нем
	 * Так как использую однобайтовую кодировку, то
	 * количество байт == количество символов
	 */

	in = fopen(filename, "rb");
	if(in == NULL){
		printf("Не получается открыть файл %s\n", filename);
		return 1;
	}
	fseek(in, 0, SEEK_END);
	length = ftell(in);
	fseek(in, 0, SEEK_SET);

	file = (char * )malloc(length + 1);
	fread(file, length, 1, in);
	fclose(in);
	return 0;
}

int Lexer::getlex(int pos, char *str)
{
	i = pos;
	rdlexems(pos, 1);
	strcpy(str, lastlex);
	return 0;

}
int  Lexer::getStringAt(int pos, char *str, int &atline )
{
	int realpos = i;
	int posInLine;
	atline = 1;
	i = pos;
	while(file[i] == '\n'){
		i--;
	}
	
	/* звиняйте */
	for(int j = 0; j < pos; j++)
		if(file[j] == '\n')
			atline++;


	if(i == length - 1){
		strncpy(str, "EOF", 4);
		return atline;
	}

	while(file[i] !='\n' && i > 0)
		i--;
	i++;


	int j = 0;
	while(file[i] !='\n'){
		str[j++] = file[i++];
		if(i == pos)
			posInLine = j;
	}
	str[j] = 0;
	i = realpos;
	return posInLine;
}
int Lexer::parseLexems()
{
	//printf("длина - %d\n", length);
	while(i < length){
		if(partly == true && lexemsread == 0){
			return last;	
		}

		if(prev != i){
			smbl = file[i];
			smbl_state = chartype(smbl);
			prev = i;
		//	printf("прочитал '%c'\n", smbl);	
			if(smbl_state == NEWLINE)
				line++;
		}
		switch(state){
			case EOF:
				return 0;
			case NONE:
				state = smbl_state;
				lexem[lexlen] = smbl;
				lexlen++;
				i++;
				break;
			/* комментарии */
			case COMMENTLINE:
				if(smbl_state == NEWLINE){
					state = COMMENTEND;
				}
				i++;
				break;
			case COMMENTBEG:
				if(smbl_state == MULTIPLY)
					state = COMMENTCANEND;
				i++;
				break;

			case COMMENTCANEND: 
				if(smbl_state != MULTIPLY && smbl_state != DIVISION)
					state = COMMENTBEG;
				else if(smbl_state == DIVISION)
					state = COMMENTEND;
				i++;
				break;
			case COMMENTEND:
				doneHere();
				break;

			case DIVISION:
				if(smbl_state == DIVISION){
					state = COMMENTLINE;
					i++;
				}else if(smbl_state == MULTIPLY){
					state = COMMENTBEG;
					i++;
				}else{
					print_info(state, lexem, line);
					doneHere();
				}
				break;

			case ID:
				if(smbl_state != ID && smbl_state != CONST10){
					if(lexlen > MAX_LEX)
						state = NAMETOOLONG;

					state = checkkeyword(lexem);
					print_info(state, lexem, line);
					doneHere();
				} else {
					lexem[lexlen] = smbl;
					lexlen++;
					i++;
				}
				
				break;
			
			case CONST10:
				/* размер должен быть меньше размера int */
				if(smbl_state != CONST10){
					noverflow = strtol(lexem, &pEnd, 10);
					/* я не знаю как проверить на переполнение иначе */
					if(noverflow > INT_MAX)
						state = CONSTTOOLONG;
					print_info(state, lexem, line);
					doneHere();
				} else {
					lexem[lexlen] = smbl;
					lexlen++;
					i++;
				}
				break;
			case BITAND:
				if(smbl_state == BITAND){
					lexem[lexlen] = smbl;
					lexlen++;
					state = LOGAND;
				}
				print_info(state, lexem, line);
				doneHere();
				i++;
				break;
			case BITOR:
				if(smbl_state == BITOR){
					lexem[lexlen] = smbl;
					lexlen++;
					state = LOGOR;
				}
				print_info(state, lexem, line);
				i++;
				doneHere();
				break;
			case BITNOT:
				print_info(state, lexem, line);
				doneHere();
				i++;

				break;
			case LOGNOT:
				if(smbl_state == ASSIGN){
					lexem[lexlen] = smbl;
					lexlen++;
					state = NEQ;
					i++;
				}
				print_info(state, lexem, line);
				doneHere();
				break;
			case MORE:
				if(smbl_state == ASSIGN){
					lexem[lexlen] = smbl;
					lexlen++;
					state = BEQ;
				}
				i++;
				print_info(state, lexem, line);
				doneHere();

				break;
			case LESS:
				if(smbl_state == ASSIGN){
					lexem[lexlen] = smbl;
					lexlen++;
					state = LEQ;
				}
					i++;

				print_info(state, lexem, line);
				doneHere();
				break;
			case ASSIGN:
				if(smbl_state == ASSIGN){
					lexem[lexlen] = smbl;
					lexlen++;
					state = EQ;
				} 
					i++;
					print_info(state, lexem, line);
					doneHere();
				break;

			case PLUS:
			case MINUS:
			case MULTIPLY:
			case LBRACKET:
			case RBRACKET:
			case LPAR:
			case RPAR:
			case MODULO:
			case DOT:
			case COLON:
			case SEMICOLON:
				print_info(state, lexem, line);
				doneHere();
				break;

			case TAB:
			case NEWLINE:
			case BACKSLASH:
			case WHITESPACE:
				doneHere();
				break;
			default:
				print_info(ERROR, lexem, line);
				doneHere();
				break;
		}
	}
	/* мне он уже не нужен, поэтому удаляю сразу */
//	free(file);
	return 0;
}

int Lexer::getPos()
{
	return i;
}
void Lexer::doneHere()
{


	strncpy(lastlex, lexem, lexlen);
	lastlex[lexlen] = 0;
	lexlen = 0;
	memset(lexem, 0, MAX_LEX);
	state = NONE;
}
int Lexer::checkkeyword(char *id)
{
	/* оппа, говнокод */
	if(!strcmp(id, "if"))
		return IF;
	else if(!strcmp(id, "else"))
		return ELSE;
	else if(!strcmp(id, "while"))
		return WHILE;
	else if(!strcmp(id, "for"))
		return FOR;
	else if(!strcmp(id, "class"))
		return CLASS;
	else if(!strcmp(id, "boolean"))
		return BOOLEAN;
	else if(!strcmp(id, "double"))
		return DOUBLE;
	else if(!strcmp(id, "true"))
		return TRUE;
	else if(!strcmp(id, "false"))
		return FALSE;
	else if(!strcmp(id, "return"))
		return RETURN;
	return ID;
}

/* вывод информации о лексеме */
void Lexer::print_info(int state, char *str, int line)
{
	/* чекнуть чтобы оно не выводило когда не нужно */
/*
	switch(state){
		case ERROR:
		case WRONGCONST10:
			printf("Неизвестные символы: '%s' тип: %d\n", str, state);
			break;
		case CONSTTOOLONG:
			printf("Кoнстанта слишком большая '%s' тип: %d\n", str, state);
			break;
		case NAMETOOLONG:
			printf("Индентификатор слишком длинный: '%s' \n", str);
			break;
		default:
			printf("'%s' тип: %d строка: %d\n", str, state, line);
			break;
	}
*/
	lexemsread--;
	if(partly == true && lexemsread == 0)
		last = state;
		
}

int Lexer::rdlexems(int pos, int howmuch)
{
	partly = true;
	i = pos;
	lexemsread = howmuch;
	last = parseLexems();
	partly = false;
	return last;
}

/* какой символ мы прочитали */
int Lexer::chartype(char c)
{
	/* все идентификаторы начинаются с буквы, если какой-то идентификатор
	 * является ключевым словом, то это проверяется только после того, как 
	 * мы закончили считывание лексемы
	 */
	if(isalpha(c))
		return ID;
	if(isdigit(c))
		return CONST10;
	switch(c){
		case '+':
			return PLUS;
		case '-':
			return MINUS;
		case '*':
			return MULTIPLY;
		case '/':
			return DIVISION;
		case '%':
			return MODULO;
		case '&':
			return BITAND;
		case '|':
			return BITOR;
		case '~':
			return BITNOT;
		case '!':
			return LOGNOT;
		case '<':
			return LESS;
		case '>':
			return MORE;
		case '=':
			return ASSIGN;
		case '(':
			return LPAR;
		case ')':
			return RPAR;
		case '{':
			return LBRACKET;
		case '}':
			return RBRACKET;
		case '.':
			return DOT;
		case ';':
			return SEMICOLON;
		case ',':
			return COLON;
		case ' ':
			return WHITESPACE;
		case '\n':
			return NEWLINE;
		case '\\':
			return BACKSLASH;
		case '\t':
			return TAB;
		default:
			return ERROR;
	}
}

