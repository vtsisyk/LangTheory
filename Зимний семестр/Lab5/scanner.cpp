#include "lexer.h"
#include "syntaxer.h"
int main(int argc, const char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Usage: %s filename\n", argv[0]);
		return 1;
	}

	Syntaxer syn(argv[1]);
	syn.checkSyntax();
}
