#ifndef SYNTAXER_H
#define SYNTAXER_H
#include "lexer.h"
#include "defs.h"
#include "syntaxtree.h"

class Syntaxer{

	public:
		Syntaxer(const char *filename);

		int checkSyntax();
		int classdef();
		int classBlock();
		int subclassBlock();
		int insideClass();
		int functionDef();
		int regularBlock();
		int functionCall();
		int ifcall();
		int manyDef();
		int submanyDef();
		int statement();
		int substatement();

		int subregularBlock();
		int arguments();
		int arguments2();
		int type();
		int operators();
		int assignment();
		int variable();
		int subvariable();

		int returncall();
		int PR2();
		int subPR2();

		int PR3();
		int subPR3();

		int PR4();
		int subPR4();

		int PR5();
		int subPR5();

		int PR6();
		int subPR6();

		int PR7();
		int subPR7();

		int PR8();
		void print_info(int pos, int expected, int got);
		void print_info(int pos);
		const char *findInEnum(int in);

		~Syntaxer();
	private:
		Lexer lex;
		int curpos; /* где мы сейчас в файле */
		int state; /* состояние */

		int statetype;
		int statetype1, statetype2;

		char name[256];
		Tree tree;
		Node *pData;
		Node *saved;
};

#endif
