#include "syntaxer.h"

Syntaxer::Syntaxer(const char *filename)
{

	int res = lex.openFile(filename);
	if(res == 1){
		exit(1);
	}
	curpos = 0;
	saved = nullptr;
}

Syntaxer::~Syntaxer()
{
	curpos = 0;
	saved = nullptr;
}

int Syntaxer::checkSyntax()
{

	int res;
	while(1){
		res = lex.rdlexems(curpos, 1);
		if(res == 0)
			return 0;
		if(res != CLASS){
			curpos = lex.getPos();
			print_info(curpos, CLASS, res);
			return 0;
		}
		if(res == CLASS){
			res = classdef();
		}
		if(res == 0){
			return 0;
		}
	}
}
int Syntaxer::classdef()
{
	int res;

	res = lex.rdlexems(curpos, 1);

	if(res != CLASS){
		print_info(curpos, CLASS, res);
		return 0;
	}

	curpos = lex.getPos();
	res = lex.rdlexems(curpos, 1);

	if(res != ID){
		print_info(curpos, ID, res);
		return 0;
	}

	lex.getlex(curpos, name);
	Node *exist = tree.checkId(name, CLASS, ROOT);
	if(exist != nullptr){
		printf("Класс с именем '%s' уже существует\n", name);
		return 0;
	}
	
	tree.addNode(nullptr, CLASS, name);
	Node *save = tree.current;
	tree.addRight();
	tree.addLeft();
	tree.goRight();
	curpos = lex.getPos();
	res = classBlock();
	tree.current = save;

	return res;
}

int Syntaxer::classBlock()
{
	int res;
	res = lex.rdlexems(curpos, 1);
	if (res != LBRACKET){
		print_info(curpos, LBRACKET, res);
		return 0;
	}
	curpos = lex.getPos();

	Node *save = tree.current;
	tree.addLeft();
	tree.goLeft();
	tree.addRight();
	tree.goRight();
	res = subclassBlock();	
	tree.current = save;
	return res;
}
int Syntaxer::subclassBlock()
{
	int res;
	res = lex.rdlexems(curpos, 1);
	if (res == RBRACKET){
		curpos = lex.getPos();
		return res;	
	} else if (res == LBRACKET){
		res = classBlock();
		if(res == 0)
			return 0;
		res = lex.rdlexems(curpos, 1);
	} else {
		res = insideClass();
		if(res == 0)
			return 0;
		res = lex.rdlexems(curpos, 1);
	}

	if(res != RBRACKET){
		res = subclassBlock();
	}
	curpos = lex.getPos();
	return res;
}

int Syntaxer::insideClass()
{
	int res; 
	res = lex.rdlexems(curpos,1);

	if(res == SEMICOLON){
		curpos = lex.getPos();
		return res;
	} else if(res == CLASS){
		res = classdef();
	} else {
		res = lex.rdlexems(curpos, 3);
		if(res == LPAR){
			res = functionDef();
		} else {
			res = manyDef();	
		}
	}
	return res;
}


int Syntaxer::functionDef()
{
	int res = lex.rdlexems(curpos, 1);
	res = type();
	if(res == 0){
		return 0;
	}

	res = lex.rdlexems(curpos, 1);

	if(res != ID){
		print_info(curpos, ID, res);
		return 0;
	}

	lex.getlex(curpos, name);

	/* имя функции */
	Node *exist = tree.checkId(name, FUNCTION, CLASS);
	if(exist != nullptr){
		print_info(curpos);
		printf("Функция '%s' уже существует\n", name);
		return 0;
	}
	
	tree.addNode(pData, FUNCTION, name);
	Node *save = tree.current;
	tree.addRight();
	tree.addLeft();
	tree.goRight();

	curpos = lex.getPos();
	res = lex.rdlexems(curpos, 1);
	if(res != LPAR){
		print_info(curpos, LPAR, res);
		return 0;
	}
	curpos = lex.getPos();
	res = arguments();

	if(res == 0)
		return res;
	
	res = lex.rdlexems(curpos, 1);
	if(res != RPAR){
		print_info(curpos, RPAR, res);
		return 0;
	}
	curpos = lex.getPos();
	res = regularBlock();
	tree.current = save;
	return res;
}

int Syntaxer::functionCall()
{
	Node *save = tree.current;
	int res = variable();
	if(res == 0){ 
		print_info(curpos, ID, res);
		return 0;
	}
	
	tree.current = pData;
	while(tree.current->getLeft()  != nullptr )
		tree.goLeft();

	res = lex.rdlexems(curpos, 1);
	if(res != ID){ 
		print_info(curpos, ID, res);
		return 0;
	}


	lex.getlex(curpos, name);
	pData = tree.checkId(name, FUNCTION, CLASS);
	if(pData == nullptr){
		print_info(curpos);
		printf("Функция '%s' не существует\n", name);
		return 0;
	} 

	int rettype  = pData->getType();


	curpos = lex.getPos();
	res = lex.rdlexems(curpos, 1);
	if(res != LPAR){ 
		print_info(curpos, LPAR, res);
		return 0;
	}
	curpos = lex.getPos();
	
	pData = pData->getRight();
	pData = pData->getLeft();

	tree.current = save;
	res = arguments2();
	
	if(res == 0)
		return res;

	res = lex.rdlexems(curpos, 1);
	if(res != RPAR){
		print_info(curpos, RPAR, res);
		return 0;
	}
	curpos = lex.getPos();

	tree.current = save;
	statetype = rettype;
	return res;
}


/* аргументы при создании функции */
int Syntaxer::arguments()
{
	int res = type();
	
	if(res == 0){
		return 0;
	} 

	res = lex.rdlexems(curpos, 1);

	if (res != ID){
		print_info(curpos, ID, 0);
		return 0;
	}

	lex.getlex(curpos, name);
	Node *exist = tree.checkId(name, PARAMETER, NONE);
	if( exist != nullptr){
		print_info(curpos);
		printf("Параметр '%s' уже существует\n", name);
		return 0;
	}
	tree.addNode(pData, PARAMETER, name);
	
	curpos = lex.getPos();
	res = lex.rdlexems(curpos, 1);

	if(res == COLON){ 
		curpos = lex.getPos();
		res = arguments();
		if(res == 0)
			return res;
	}
	return res;
}


bool doable(int a, int b)
{
	if((a == DOUBLE && b == CONST10)  ||
		(a == CONST10 && b == DOUBLE) ||	
		(a == DOUBLE && b == DOUBLE) ||
		(a == CONST10 && b == CONST10) ||
		(a == BOOLEAN && b == BOOLEAN))
		return true;
	return false;
}
int Syntaxer::arguments2()
{

	Node *fargs = pData;
	Node *tre = tree.current;
	int res = statement();
	if(res == 0)
		return 0;
	pData = fargs;
	tree.current = tre;
	/* проверить количество аргументов */

	Node *exist = pData;
	if(exist->getNodeType() != PARAMETER){
		print_info(curpos);
		printf("Неверное количество аргументов \n");
		return 0;
	}



	if(!doable(exist->getType(), statetype)){
		print_info(curpos, exist->getType(), statetype);
		printf("неверный тип '%s' \n", name);
		return 0;
	}
	res = lex.rdlexems(curpos, 1);

	if(res == COLON){
		curpos = lex.getPos();
		pData = pData->getLeft();
		res = arguments2();
		return res;
	}

	if(exist->getLeft()->getNodeType() == PARAMETER){
		print_info(curpos);
		printf("Недостаточное число параметров\n");
		return 0;
	}
	
	
	return res;
}

 
int Syntaxer::regularBlock()
{
	int res;

	res = lex.rdlexems(curpos, 1);
	if (res != LBRACKET){
		print_info(curpos, LBRACKET, res);
		return 0;	
	}
	curpos = lex.getPos();

	tree.addLeft();
	tree.goLeft();
	tree.addRight();
	tree.goRight();
	Node *save = tree.current;

	res = subregularBlock();
	tree.current = save;
	return res;
}

int Syntaxer::subregularBlock()
{
	
	int	res = lex.rdlexems(curpos, 1);
	if (res == RBRACKET){
		curpos = lex.getPos();
		return res;	
	}
	/* хз как отделить класс от нормальной переменной */
	else if(res == BOOLEAN || res == DOUBLE || res == ID){
		
		res = lex.rdlexems(curpos, 2);
		if(res == ID){
			res = manyDef();
		} else {
			res = operators();
		}
		if(res == 0)
			return 0;
		res = subregularBlock();
	} else {
		res = operators();
		if(res == 0)
			return 0;
		res = subregularBlock();
	}
	return res;

}
int Syntaxer::type()
{
	int res = lex.rdlexems(curpos, 1);

	if(res != BOOLEAN && res != DOUBLE && res != ID){
		print_info(curpos, TYPE, 0);
		return 0;
	}
	if(res == BOOLEAN){
		pData = tree.getBooleanNode();;
	} else if(res == DOUBLE)
		pData = tree.getDoubleNode();

	if(res == ID){
		while(true){
			lex.getlex(curpos, name);
			pData = tree.searchUp(name, CLASS);
			if(pData == nullptr){
				print_info(curpos);
				printf("Класса '%s' не существует\n", name);
				return 0;
			}
			res = lex.rdlexems(curpos, 2);
			if(res != DOT){
				res = lex.rdlexems(curpos, 1);
				break;
			}
			curpos = lex.getPos();
			tree.current = pData;
			tree.goRight();
			if(tree.current == nullptr){
			
			lex.getlex(curpos, name);
				print_info(curpos);
				printf("Класса '%s' не существует\n", name);
				return 0;
			}
			tree.goLeft();
			tree.goRight();
			tree.goLeft();
			while(tree.current->getLeft() != nullptr)
				tree.goLeft();
		}
	}
	curpos = lex.getPos();
	return res;
}

int Syntaxer::ifcall()
{

	int res = lex.rdlexems(curpos, 1);
	if(res != IF){
		print_info(curpos, IF, res);
		return 0;
	}
	curpos = lex.getPos();

	res = lex.rdlexems(curpos, 1);
	if(res != LPAR){
		print_info(curpos, LPAR, res);
		return 0;
	}
	curpos = lex.getPos();

	res = statement();
	if(res == 0){
		return res;
	}

	if(statetype != BOOLEAN){
		
		print_info(curpos, BOOLEAN, statetype);
		printf("Ожидалось логическое выражение\n");
		return 0;
	}
	res = lex.rdlexems(curpos, 1);
	if(res != RPAR){
		print_info(curpos, RPAR, res);
		return 0;
	}

	curpos = lex.getPos();
	
	res = operators();
	if(res == 0){
		return res;
	}

	res = lex.rdlexems(curpos, 1);

	if(res == ELSE){
		curpos = lex.getPos();
		res = operators();
	}

	return res;
}

int Syntaxer::operators()
{
	int res = lex.rdlexems(curpos, 1);
	if(res == LBRACKET){
		res = regularBlock();
	} else if (res == SEMICOLON){
		curpos = lex.getPos();
	} else if(res == IF){
		res = ifcall();
	} else if(res == RETURN){
		res = returncall();
	} else {
		res = assignment();
		if(res == 0)
			return res;
	}
	return res;
}
int Syntaxer::returncall()
{
	int res = lex.rdlexems(curpos, 1);
	if(res != RETURN){
		print_info(curpos, RETURN, res);
		return 0;
	}

	curpos = lex.getPos();
	res = statement();
	pData = tree.searchUp(FUNCTION);

	if(res == 0)
		return res;
	if(pData->getType() != statetype){
		print_info(curpos, statetype, pData->getType());
		printf("возвращаемый тип не совпадает с типом функции\n" );
		return 0;
	}



	/* проверить выражение и тип, который должен вернуться */
	res = lex.rdlexems(curpos,1);
	if(res != SEMICOLON){
		print_info(curpos, SEMICOLON, res);
		return 0;
	}

	curpos = lex.getPos();
	return res;

}
int Syntaxer::assignment()
{
	int res = variable();
	int tmp = statetype;
	if(res == 0)
		return res;
	res = lex.rdlexems(curpos, 1);

	if(res != ASSIGN){
		print_info(curpos, ASSIGN, res);
		return 0;
	}
	curpos = lex.getPos();
	res = statement();
	if(res == 0)
		return res;
	
	res = lex.rdlexems(curpos, 1);

	if(res != SEMICOLON){
		print_info(curpos, SEMICOLON, res);
		return 0;
	}

	statetype1 = tmp;
	statetype2 = statetype;

	if((statetype1 != BOOLEAN && statetype2 == BOOLEAN) || 
	   (statetype1 == BOOLEAN && statetype2 != BOOLEAN) || 
	   (statetype1 != CLASS && statetype2 == CLASS) ||
	   (statetype1 == CLASS && statetype2 != CLASS)){

		print_info(curpos, statetype1, statetype2);
		printf("Нельзя выполнить присваивание\n");
		return 0;
	}
	curpos = lex.getPos();

	return res;
}

int Syntaxer::variable()
{
	int res;
	Node *save = tree.current;
	res = lex.rdlexems(curpos, 1);

	if(res != ID){
		print_info(curpos, ID, res);
		return 0;
	}

	int tmp = lex.getPos();
	res = lex.rdlexems(curpos, 2);
	if(res == LPAR){
		pData = tree.current;
		return FUNCTION;
	}
	
	lex.getlex(curpos, name);

	while(tree.current->getLeft()  != nullptr )
		tree.goLeft();


	Node *par = tree.searchUp(name, PARAMETER);
	Node *var = tree.searchUp(name, VARIABLE);
	Node *clas = tree.searchUp(name, CLASS);
/*
	Node *par = tree.checkId(name, PARAMETER, FUNCTION);
	Node *var = tree.checkId(name, VARIABLE, CLASS);
	Node *clas = tree.checkId(name, CLASS, ROOT);
*/
	if(var == nullptr && par == nullptr && clas == nullptr){ 
		print_info(curpos);
		printf("Переменной '%s' не существует\n", name);
		return 0;
 	}

	if(par != nullptr){
		pData = par;
	}else if(var !=nullptr){
		pData = var;
	} else 
		pData = clas;
	statetype = pData->getType();
	curpos = tmp; 
	res = subvariable();

	tree.current = save;

	return res;
	
} 
int Syntaxer::subvariable()
{

	int res = lex.rdlexems(curpos, 1);

	if(res != DOT){
		return res;
	}

/*
 *  a 
 *    \
 *     *
 *    /
 *   *              |
 *    \             | блок
 *     *            |
 *    /             |
 *   *              |

 */

	pData = pData->getpType();
	tree.current = pData;
	tree.goRight();
	tree.goLeft();
	tree.goRight();
	tree.goLeft();


	curpos = lex.getPos();

	res = lex.rdlexems(curpos, 1);

	if(res != ID){
		print_info(curpos, ID, res);
		return 0;
	}
	int tmp = lex.getPos();
	res = lex.rdlexems(curpos, 2);

	pData = tree.current;
	if(res == LPAR){
		res = lex.rdlexems(curpos, 1);
		lex.getlex(curpos, name);
		pData = tree.searchDownLeft(name, FUNCTION);
	
		if(pData == nullptr){
			print_info(curpos);
			printf("Не удалось найти функцию с именем '%s'\n", name);
			return 0;
		}
		pData = pData->getRight();
		pData = pData->getLeft();
		while(pData->getNodeType() == PARAMETER)
			pData = pData->getLeft();
		return FUNCTION;
	}

	lex.getlex(curpos, name);
	pData = tree.searchDownLeft(name, VARIABLE);
	if(pData == nullptr){

		print_info(curpos);
		printf("Переменная '%s' не найдена\n", name);
		return 0;
	}

	statetype = pData->getType();
	curpos = tmp; 
	res = subvariable();
	return res;

}

int Syntaxer::manyDef()
{
	
	int res = type();

	if(res == 0)
		return res;
	
	res = submanyDef();
	return res;
}

int Syntaxer::submanyDef()
{
	int	res = lex.rdlexems(curpos, 1);
	if(res != ID){
		print_info(curpos, ID, res);
		return 0;
	}
	lex.getlex(curpos, name);
	
	Node *exist = tree.checkId(name, VARIABLE, NONE);
	if(exist != nullptr){

		print_info(curpos);
		printf("Переменная '%s' уже объявлена\n", name);
		return 0;
	}
	
	tree.addNode(pData, VARIABLE, name);
	curpos = lex.getPos();

	res = lex.rdlexems(curpos, 1);

	int tmp = statetype;
	if(res == ASSIGN){
		curpos = lex.getPos();
		res = statement();

		if(res == 0)
			return 0;
	statetype2 = statetype;
	statetype1 = tmp;
	if((statetype1 != BOOLEAN && statetype2 == BOOLEAN) || 
	   (statetype1 == BOOLEAN && statetype2 != BOOLEAN) || 
	   (statetype1 != CLASS && statetype2 == CLASS) ||
	   (statetype1 == CLASS && statetype2 != CLASS)){
	   	
			print_info(curpos, statetype1, statetype2);
			printf("Нельзя выполнить присваивание\n");
			return 0;
	}
		res = lex.rdlexems(curpos, 1);
	}
	if(res == COLON){
		curpos = lex.getPos();
		res = submanyDef();
	} else if(res == SEMICOLON){
		curpos = lex.getPos();
		return res;
	} else {
		print_info(curpos, 0 ,res);
	}

	return res;
}
int Syntaxer::statement()
{
	int res = PR2();
	if(res == 0)
		return res;
	statetype1 = statetype;
	res = substatement();

	return res;
}

int Syntaxer::substatement()
{

	int res = lex.rdlexems(curpos, 1);
	if(res == LOGOR || res == BITOR){
		curpos = lex.getPos();
	} else {
		return res;
	} 

	res = PR2();
	if(res == 0)
		return res;

	statetype2 = statetype;
	if(statetype1 != BOOLEAN || statetype2 != BOOLEAN){
		print_info(curpos, statetype1, statetype2);
		printf("Недопустимая операция\n");
		return 0;
	} else {
		statetype = BOOLEAN;
	}
	res = substatement();
	return res;
}

int Syntaxer::PR2()
{
	int res = PR3();
	if(res == 0)
		return res;
	statetype1 = statetype;
	res = subPR2();
	return res;
}
int Syntaxer::subPR2()
{

	int res = lex.rdlexems(curpos, 1);
	if(res == LOGAND || res == BITAND){
		curpos = lex.getPos();
	} else {
		return res;
	}

	res = PR3();
	if(res == 0)
		return res;
	statetype2 = statetype;
	if(statetype1 != BOOLEAN || statetype2 != BOOLEAN){

		print_info(curpos, statetype1, statetype2);
		printf("Недопустимая операция\n");
		return 0;
	} else {
		statetype = BOOLEAN;
	}

	res = subPR2();
	return res;
}

int Syntaxer::PR3()
{
	int res = PR4();
	if(res == 0)
		return res;
	statetype1 = statetype;
	res = subPR3();
	return res;
}
int Syntaxer::subPR3()
{
	int res = lex.rdlexems(curpos, 1);
	if(res == EQ || res == NEQ){
		curpos = lex.getPos();
	} else {
		return res;
	}

	res = PR4();

	statetype2 = statetype;
	if(res == 0)
		return res;
	if(statetype1 == DOUBLE && statetype2 == DOUBLE){
		statetype = BOOLEAN;
	} else if(statetype1 == CONST10 || statetype2 == CONST10){
		statetype = BOOLEAN;
	} else if((statetype1 == DOUBLE && statetype2 == CONST10) ||
			   (statetype1 == CONST10 && statetype2 == DOUBLE)){
		statetype = BOOLEAN;
	} else {

		print_info(curpos, statetype1, statetype2);
		printf("Недопустимая операция\n");
		return 0;
	}

	res = subPR3();
	return res;
}


int Syntaxer::PR4()
{
	int res = PR5();
	if(res == 0)
		return res;
	statetype1 = statetype;
	res = subPR4();
	return res;
}
int Syntaxer::subPR4()
{
	int res = lex.rdlexems(curpos, 1);
	if(res == LESS || res == MORE || res == BEQ || res == LEQ){
		curpos = lex.getPos();
	} else {
		return res;
	}

	res = PR5();
	if(res == 0)
		return res;

	statetype2 =  statetype;
	if(statetype1 == DOUBLE && statetype2 == DOUBLE){
		statetype = BOOLEAN;
	} else if(statetype1 == CONST10 || statetype2 == CONST10){
		statetype = BOOLEAN;
	} else if((statetype1 == DOUBLE && statetype2 == CONST10) ||
			   (statetype1 == CONST10 && statetype2 == DOUBLE)){
		statetype = BOOLEAN;
	} else {

		print_info(curpos, statetype1, statetype2);
		printf("Недопустимая ооперация\n");
		return 0;
	}
	
	res = subPR4();
	return res;
}

int Syntaxer::PR5()
{
	int res = PR6();
	if(res == 0)
		return res;
	statetype1 = statetype;
	res = subPR5();
	return res;
}
int Syntaxer::subPR5()
{
	int res = lex.rdlexems(curpos, 1);
	if(res == PLUS || res == MINUS){
		curpos = lex.getPos();
	} else {
		return res;
	}

	statetype2 = statetype;
	res = PR6();
	if(res == 0)
		return res;
	if(statetype1 == DOUBLE && statetype2 == DOUBLE){
		statetype = DOUBLE;
	} else if(statetype1 == CONST10 || statetype2 == CONST10){
		statetype = CONST10;
	} else if((statetype1 == DOUBLE && statetype2 == CONST10) ||
			   (statetype1 == CONST10 && statetype2 == DOUBLE)){
		statetype = DOUBLE;
	} else {

		print_info(curpos, statetype1, statetype2);
		printf("Недопустимая операция\n");
		return 0;
	}
	res = subPR5();
	return res;
}


int Syntaxer::PR6()
{
	int res = PR7();
	if(res == 0)
		return res;
	statetype1 = statetype;
	res = subPR6();
	return res;
}
int Syntaxer::subPR6()
{
	int res = lex.rdlexems(curpos, 1);
	if(res == MULTIPLY || res == DIVISION || res == MODULO){
		curpos = lex.getPos();
	} else {
		return res;
	}

	res = PR7();
	statetype2 = statetype;
	if(res == 0)
		return res;
	if(statetype1 == DOUBLE && statetype2 == DOUBLE){
		statetype = DOUBLE;
	} else if(statetype1 == CONST10 || statetype2 == CONST10){
		statetype = CONST10;
	} else if((statetype1 == DOUBLE && statetype2 == CONST10) ||
			   (statetype1 == CONST10 && statetype2 == DOUBLE)){
		statetype = DOUBLE;
	}
	/* поправь чекалку */
	if(statetype1 != DOUBLE || statetype2 != DOUBLE){

		print_info(curpos, statetype1, statetype2);
		printf("Недопустимая ооперация\n");
		return 0;
	}
	res = subPR6();
	return res;
}
/*
int Syntaxer::PR7()
{
	int res = PR8();
	if(res == 0)
		return res;
	res = subPR7();
	return res;
}
*/
int Syntaxer::PR7()
{
	int res = lex.rdlexems(curpos, 1);
	if(res == LOGNOT || res == BITNOT){
		curpos = lex.getPos();
		res = PR8();
		statetype1 = statetype;
		if(statetype1 != BOOLEAN){

			print_info(curpos, BOOLEAN, statetype1);
			printf("Недопустимая операция\n");
			return 0;
		} else {
			statetype = BOOLEAN;
		}
		if(res == 0)
			return res;
	} else {
	
		res = PR8();
	}
	return res;
}
int Syntaxer::PR8()
{
	int res = lex.rdlexems(curpos, 1);
	if(res == CONST10){
		curpos = lex.getPos();
		statetype = CONST10;
		return res;
	} else if(res == FALSE || res == TRUE){
		curpos = lex.getPos();
		statetype = BOOLEAN;
		return res;
	} else if(res == ID){
		int pos = curpos;
		int tmp = variable();
		if(tmp == 0)
			return 0;
		curpos = pos;
		if(tmp == FUNCTION){
			res = functionCall();
		} else {
			res = variable();
		}
	} else if(res == LPAR){
		curpos = lex.getPos();
		res = statement();
		if(res == 0)
			return res;
		res = lex.rdlexems(curpos, 1);
		if(res != RPAR){
			print_info(curpos, RPAR, res);
			return 0;
		} 
		curpos = lex.getPos();
	} else {
		/*
		if(res == LOGNOT || res == BITNOT || res == PLUS || res == MINUS){
			return res;
		*/
		print_info(curpos, ID,res);
		return 0;
	} 
	return res;
}


/* блоки с приоритетами сделай */

void Syntaxer::print_info(int pos, int expected, int got)
{
	int line;
	int posInLine;
	char str[256];
	posInLine = lex.getStringAt(pos, str, line);
	printf("ЕГГОГ: %s\nстрока: %d, %d\n", str, line, posInLine);
	printf("Ожидалось: %s\nПолучил: %s\n", findInEnum(expected), findInEnum(got));
}

void Syntaxer::print_info(int pos)
{
	int line;
	int posInLine;
	char str[256];
	posInLine = lex.getStringAt(pos, str, line);
	printf("ЕГГОГ: %s\nстрока: %d, %d\n", str, line, posInLine);
}
const char *Syntaxer::findInEnum(int in){
	switch(in){
		case 0:
			return "";
		case ID:
			return "Id";
		case CLASS:
			return "class";
		case TYPE:
			return "type";
		case DOUBLE:
			return "double";
		case CONST10:
			return "константа";
		case BOOLEAN:
			return "boolean";
		case RETURN:
			return "return";
		case PLUS:
			return "+";
		case MINUS:
			return "-";
		case MULTIPLY:
			return "*";
		case DIVISION:
			return "/";
		case MODULO:
			return "%";
		case BITAND:
			return "&";
		case BITOR:
			return "|";
		case BITNOT:
			return "~";
		case LOGNOT:
			return "!";
		case LESS:
			return "<";
		case MORE:
			return ">";
		case ASSIGN:
			return "=";
		case LPAR:
			return "(";
		case RPAR:
			return ")";
		case LBRACKET:
			return "{";
		case RBRACKET:
			return "}";
		case DOT:
			return ".";
		case SEMICOLON:
			return ";";
		case COLON:
			return ",";
		default:
			return " ";	
	}

	return "";
}



