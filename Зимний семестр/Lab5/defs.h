#ifndef STATES_H 
#define STATES_H

#define MAX_LEX 4096
#define ID_LEN 256
#define MAX_FILESIZE 100500

/* решил их вынести в отдельный enum, думаю, что это имеет смысл */
enum states{
	/* лексемы */
	ID = 1,           /* идентификатор */
	CONST10,      /* десятичная константа */

	/* арифметические операции */
	
	PLUS,         /* + */
	MINUS,        /* - */
	DIVISION,     /* / */
	MULTIPLY,     /* * */
	MODULO,       /* % */
	ASSIGN,       /* = */

	/* битовые операции */	
	BITAND,       /* & */
	BITOR,        /* | */
	BITNOT,       /* ~ */
	
	/* логические операции */
	
	LOGAND,       /* && */
	LOGOR,        /* || */
	LOGNOT,       /* ! */
	LESS,         /* < */
	MORE,         /* > */
	BEQ,          /* >= */
	LEQ,          /* <= */
	NEQ,          /* != */
	EQ,           /* == */
	
	/* ключевые слова  */
	
	IF,           /* if */
	ELSE,         /* ELSE */
	WHILE,        /* while */
	FOR,          /* for */
	CLASS,        /* class */
	BOOLEAN,      /* boolean */
	DOUBLE,       /* double */
	TRUE,         /* true */
	FALSE,        /* false */
	RETURN,       /* return */
	
	/* скобки  */
	LPAR,         /* ( */
	RPAR,         /* ) */
	LBRACKET,     /* { */
	RBRACKET,     /* } */

	/* разделители */
	DOT,          /* . */
	SEMICOLON,    /* ; */
	COLON,        /* , */
	WHITESPACE,   /* ' ' */


	/* комментарий */
	COMMENTLINE,  /* // комментарий на всю строку */
	COMMENTBEG,   /* начало многострочного комментария */
	COMMENTCANEND,/* любые символы между начало и концом комментария */
	COMMENTEND,   /* конец многострочного комментария */


	/* типы узлов */
	FUNCTION,
	PARAMETER,
	VARIABLE, 
	ROOT,

	/* Спецсимволы */
	WRONGCONST10, /* ну что-то типа 10qwerty */
	NONE,         /* исходное состояние */
	NEWLINE,      /* \n */
	TAB,          /* \t */
	BACKSLASH,    /* \ */
	CONSTTOOLONG,
	NAMETOOLONG,
	TYPE,
	ERROR,        /* неизвестный символ */
};

#endif /* ifndef STATES_H */
