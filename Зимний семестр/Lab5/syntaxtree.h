#include "defs.h"
#include <string.h>
class Node;

union type_t{
	int selected;
	int retype; /* double, boolean */
	Node *usrtype; /* переменная класса*/
};

struct varident{
	int vartype; /* переменная/константа*/
};

struct funcident{
	int varc;
};

struct classident{
};
class Node
{
	public:
		Node();
		void setLeft(Node *node);
		void setRight(Node *node);

		void addLeft();
		void addRight();
		void addNode(Node *pType,int nodetype ,char *name);

		bool nameExist(char *name);
		bool isThisNode(char *name, int type);
		bool isFunction();
		bool isClass();
		int getNodeType();	
		int getType();
		void setType(int type);

		char*getClassName();
		Node *getLeft();
		Node *getRight();
		Node *getpType();
		Node *getParent();
		Node *memalloc();
	protected:
		
		char name[256]; /* имя узла */
		int nodetype;	/* тип узла(функция, класс, переменная) */
		Node *vartype;  /* тип переменной(double, boolean, класс )*/
		
		Node *parent;
		Node *left;
		Node *right;
};
class Tree: public Node 
{
	public:
		Tree();
		void createRoot();
		void goLeft();
		void goRight();
		
		Node *searchParentNode();
		void addLeft();
		void addRight();
		void addNode(Node *pType,int nodetype ,char *name);

		Node *searchDownLeft(char *name, int type);
		Node *checkId(char *name, int type, int stop);
		Node *searchUp(char *name, int type);
		Node *searchUp(int type);
		void deleteTree();
		Node *findUp(); /**/
		Node *getBooleanNode();
		Node *getDoubleNode();
		
		Node *current;
	private:
		Node *root; /* корень дерева */
		Node doubleNode; /* специальный узел для типа double */
		Node booleanNode;
};



