#include "syntaxtree.h"
Tree::Tree()
{
	root = new Node();
	current = root;

	booleanNode.setType(BOOLEAN);
	doubleNode.setType(DOUBLE);
}

void Tree::createRoot()
{
}

Node::Node()
{

	left = right = nullptr;
	nodetype = NONE;
}

Node *Node::getLeft()
{
	return left;
}

Node *Node::getRight()
{
	return right;
}

Node *Node::getParent()
{
	return parent;
}

void Node::setType(int type)
{
	this->nodetype = type;
}
void Tree::goLeft()
{
	current = current->getLeft();
}
void Tree::goRight()
{
	current = current->getRight();

}

char *Node::getClassName()
{
	return name;
}

void Node::addNode(Node *pType, int nodetype, char *name)
{
	Node *n = new Node();
	n->vartype = pType;
	n->nodetype = nodetype;
	strcpy(n->name, name);
	setLeft(n);
}

void Node::setLeft(Node *node)
{
	node->parent = this;
	this->left = node;
}
void Node::setRight(Node *node)
{
	node->parent = this;
	this->right = node;
}

void Node::addLeft()
{
	Node *node = new Node();
	node->parent = this;
	this->left = node;
}
void Node::addRight()
{
	Node *node = new Node();
	node->parent = this;
	this->right = node;
}

void Tree::addRight()
{
	current->addRight();
}

void Tree::addLeft()
{
	current->addLeft();
}

void Tree::addNode(Node *pType, int nodetype, char *name)
{
	current->addNode(pType, nodetype, name);
	goLeft();
}
Node *Node::memalloc()
{
	Node *node = new Node();
	/* проверить выделилось или нет */

	return node;
}


bool Node::isThisNode(char *name, int type)
{
	if(this->nodetype == type && !strcmp(this->name, name)){
		return true;
	} else {
		return false;
	}
}
bool Node::nameExist(char *name)
{
	if(!strcmp(this->name, name)){
		return true;
	} else {
		return false;
	}
}
bool Node::isFunction()
{
	if(this->nodetype == FUNCTION)
		return true;
	else 
		return false;
}
bool Node::isClass()
{
	if(this->nodetype == CLASS)
		return true;
	else 
		return false;
}


int Node::getType()
{
	if(this->nodetype == CLASS){
		return CLASS;
	} else {
		return this->vartype->nodetype;
	}
	return NONE;
}
int Node::getNodeType()
{
	return this->nodetype;
}

Node * Node::getpType()
{
	return this->vartype;
}
Node * Tree::getBooleanNode()
{
	return &booleanNode;
}
Node * Tree::getDoubleNode()
{
	return &doubleNode;
}
Node *Tree::searchUp(char *name, int type)
{
	Node *tmp = current;
	while(tmp != root){
		if(tmp->nameExist(name) && (tmp->getNodeType() == type || type == NONE)){
			return tmp;
		} else {
			tmp = tmp->getParent();
		}
	}
	return nullptr;

}
Node *Tree::searchUp(int type)
{
	Node *tmp = current;
	while(tmp != root){
		if((tmp->getNodeType() == type || type == NONE)){
			return tmp;
		} else {
			tmp = tmp->getParent();
		}
	}
	return nullptr;

}
Node *Tree::searchDownLeft(char *name, int type)
{
	Node *tmp = current;
	while(tmp != nullptr){
		if(tmp->nameExist(name) && (tmp->getNodeType() == type || type == NONE)){
			return tmp;
		} else {
			tmp = tmp->getLeft();
		}
	}
	return nullptr;

}

Node *Tree::searchParentNode()
{
	Node *tmp = current;
	bool folded = false;
	while( tmp != root){
		if(tmp->getNodeType() == CLASS && folded){
			return tmp;
		}
		if(tmp->getParent()->getRight() == tmp){
			folded = true;
		}else{
			folded = false;
		}

		tmp = tmp->getParent();
	}
	return nullptr;

}
/* ищем узел с таким именем, до такого типа узла*/
Node *Tree::checkId(char *name, int type, int stop)
{
	Node *tmp = current;
	bool folded = false;
	while( tmp != root){
		if(tmp->nameExist(name) && tmp->getNodeType() == type){
			if(type == CLASS && folded)
				return tmp;
			else if(!(type == CLASS && !folded)){
				return tmp;
			}
		}
	
		if(tmp->getParent()->getRight() == tmp){
			folded = true;
		}else{
			folded = false;
		}
		tmp = tmp->getParent();
		

		if(tmp->getNodeType() == stop){
			if(stop == CLASS && folded)
				break;
			else if(stop != CLASS)
				break;
		}
	}
	return nullptr;
}
void Tree::deleteTree()
{

	/* запилить высвобождение памяти */
}
