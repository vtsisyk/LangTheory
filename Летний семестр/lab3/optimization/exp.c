#include <stdio.h>

int f(int c)
{
    if (c - 7 > c)
        return c + 1;
    else
        return c;
}
void swap(int *a, int *b)
{
    int c = *a;
    *a = *b;
    *b = c;
}

int main(int argc, char *argv[])
{
    /* замена константой, а будет 8*/
    int a = 2 + 2 + 2 + 2;
    printf("%d\n", a);

    /* заменить вычисление константой */
    int b = 0;
    b = a + a;
    printf("%d\n", b);

    /* заменить вычисление константой */
    int c = (3 * b / 2 / a * a - (b % a)) << 1;
    printf("%d\n", c);

    /* сделает сдвиг */
    c = c * 2;
    printf("%d\n", c);

    /* сделает сдвиг и вычтет  */
    c = c * (3 * b << 2 / 2 / a * a - (b % a)) << 1;
    c = c * (3 * b << 2 / 2 / a * a - (b % a)) << 1;
    printf("%d\n", c);

    c = f(a);
    printf("%d", c);

    swap(&a, &b);

    printf("%d", a);
    return 0;
}
