#include <stdio.h>

void func1(const char input) { printf("func1: %c\n", input); }

void func2(const char input) { printf("func2: %c\n", input); }

void func3(const char input) { printf("func3: %c\n", input); }

int main()
{
    char input = (char)getchar();

    if (input == 'Y') {
        func1(input);
    }

    func2(input);

    if (input == 'Y') {
        func3(input);
    }
}
