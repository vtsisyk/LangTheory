#include <stdio.h>

/* недостижимый код*/
void function_name() { printf("hello world\n"); }

int main(int argc, char *argv[])
{
    int b = 0;

    /* бесполезный цикл */
    for (int i = 0; i < 10; ++i) {
        b++;
    }

    printf("%d", b);
    return 0;
}
