/* прога для демонстрации выравнивания в структурах */
#include <stdio.h>

int main()
{
        char str[10] = {'a', 0, 0, 0, 0, 'b', 0, 0, 0, 0};
        struct test {
                char a;
                int b;
        } * ptr;

        ptr = (struct test *)str;
        *ptr++;

        struct __attribute__((__packed__)) test_align {
                char a;
                int b;
        } * ptr2;

        ptr2 = (struct test_align *)str;
        *ptr2++;
        return 0;
}
