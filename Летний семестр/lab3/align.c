/* прога для демонстрации выравнивания в структурах */
#include <stdio.h>

int main()
{
        char str[10] = {'a', 1, 1, 1, 1, 'b', 1, 1, 1, 1};
        struct test {
                char a;
                int b;
        } * ptr;

        printf("размер: %d\n", sizeof(struct test));
        ptr = (struct test *)str;
        printf("%s\n", ptr);
        printf("1 -- %c \n", ptr->a);
        *ptr++;
        printf("2 -- %c\n", ptr->a);

        struct __attribute__((__packed__)) test_align {
                char a;
                int b;
        } * ptr2;

        ptr2 = (struct test_align *)str;
        printf("%s\n", ptr2);
        printf("размер: %d\n", sizeof(struct test_align));
        printf("1 -- %c \n",  ptr2->a);
        *ptr2++;
        printf("2 -- %c \n",  ptr2->a);

        return 0;
}
