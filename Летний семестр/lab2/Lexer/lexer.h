#ifndef LEXER_H
#define LEXER_H
#include <ctype.h>
#include <limits.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cctype>
#include <string>
#include <unordered_map>
#include "defs.h"

typedef struct position {
	int line;
	int offset;
	int byte;
} position_t;


typedef struct lexem {
	position_t position;
	std::string lexem;
	std::string typestr;
	TokenType type;
} lexem_t;


class Lexer {
    public:
	lexem_t next(); /* read lexem */
	bool openFile(const std::string &filename);
	Lexer();
	Lexer(std::string text, std::string type);
	std::string getCurLine();
	std::string getTypeName(TokenType ton) { return tokenMap[ton]; }
	void setPos(position_t position) { cursor = position; }
	position_t getPos() { return cursor; }
//	~Lexer();

	lexem_t current(position_t);
    protected:
	void skip();	  /* skip all unnecessary symbols and */
	char *file = nullptr; /* source code file */
	int size;	     /* size of the file */
	int lexlen;	   /* fize of lexem_t */

	position_t cursor;

	lexem_t lex;
	std::string buffer;

	typedef std::unordered_map<TokenType, std::string> TokenToStringMap;
	TokenToStringMap tokenMap;
	typedef std::unordered_map<std::string, TokenType> StringToToken;
	StringToToken keywordMap;

    private:
	bool isInit = false; /* we don't want to initialize maps twice */
	lexem_t parseNumber();
	lexem_t parseID();
	lexem_t parseOperator();

	bool init(); /* initializes maps of keywords. string to TokenType and
			backwards */
	bool isLetter(char ch);
	bool isNumber(char ch);

	char getChar();
	void ungetChar(char ch);
};

#endif /* ifndef LEXER_H */
