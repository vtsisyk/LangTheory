#include "node.h"

Node::Node()
{
	type = NodeType::UNKNOWN;
	left = right = parent = nullptr;

	info = 0;
}

Node::Node(NodeType t)
{
	type = t;
	left = right = parent = nullptr;
}
bool Node::setLeft(Node *node)
{
	if (left != nullptr)
		return false;
	node->setParent(this);
	left = node;
	return true;
}

bool Node::setRight(Node *node)
{
	if (right != nullptr)
		return false;
	node->setParent(this);
	right = node;
	return true;
}

bool Node::setParent(Node *node)
{
	if (parent != nullptr)
		return false;
	parent = node;
	return true;
}

void Node::addInfo(int token) { info = info | token; }
void Node::rmInfo(InfoType token)
{
	if (info & token) {
		info = info ^ token;
	}
}
bool Node::isset(int token) { return info & token; }


void Node::rmBelow(){

	if(this->left !=nullptr){
		this->left->rmBelow();
	}

	if(this->right !=nullptr){
		this->right->rmBelow();
	}

	/* delete dis */
	delete this;
}

Node *Node::copyNode()
{

	Node *node = new Node();

	node->type = this->type;
	node->info = this->info;
	node->lex = this->lex;
	node->value = this->value;

	return node;
}

Node *Node::copySubtree()
{
	Node *node = this->copyNode();
	/* copy all info */

	if(this->left != nullptr)
		node->setLeft(this->left->copySubtree());
	else 
		node->setLeft(this->copyNode());

	if(this->right != nullptr)
		node->setRight(this->right->copySubtree());
	else 
		node->setRight(this->copyNode());

	return node;
}

Node::~Node(){

}
void Node::setValue(type_t val){
	if(type == NodeType::VARIABLE |type == NodeType::PARAMETER){
		value.node_type->value = val;
	} else{
		value = val;
	}
}
	
type_t Node::getValue(){
	if(type == NodeType::VARIABLE |type == NodeType::PARAMETER){
		return value.node_type->value;
	} else {
		return value;
	}

}
