#ifndef SYNTAXTREE_H
#define SYNTAXTREE_H

#include <string>
#include "node.h"
class SyntaxTree {
    private:
	Node *root;
	Node *current;

	// TODO: maybe i should move tmp nodes from syntaxer.h to here

    public:
	/*!
	 temporary node to save info about found node in sntx_variable. I'm
	 sorry for this
	 */
	Node *found;

	Node *getLeft();
	Node *getRight();

	Node *getRoot() { return root; }
	Node *getCurrent() { return current; }
	void setCurrent(Node *node) { current = node; }
	void goLeft();
	void goRight();
	void goUp();
	Node *searchUp(std::string, int, int);

	Node *searchUp(int stop, int nodes);
	SyntaxTree();
};

#endif /* SYNTAXTREE_H */
