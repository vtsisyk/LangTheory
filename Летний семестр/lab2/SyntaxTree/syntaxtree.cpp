#include "syntaxtree.h"

/* TODO: better search */
SyntaxTree::SyntaxTree()
{
	root = new Node(NodeType::ROOT);

	current = root;
	found = nullptr;
}

/* search up */
Node *SyntaxTree::searchUp(std::string name, int stop, int nodes)
{
	Node *tmp = current;
	Node *previous = current;
	bool parent = false;
	/*
	 *           c - some class
	 *            \
	 *             *
	 *            /
	 *           v - some variable
	 *          /
	 *         a - can be also class
	 *        /
	 *       b - can be function
	 *      /
	 *     v - new variable we want to attach. Error
	 *
	 * We should check if name 'v' was already taken in this scope.
	 * For lowest 'v' we should go up until we find parent
	 * class or method node.
	 *
	 *
	 * Usually we check by name, so std::string should be enough
	 * */

	while (!parent || !(stop & tmp->getType())) {
		if (tmp == root)
			return root;

		if (tmp->getName() == name && nodes & tmp->getType()) {
			return tmp;
		}
		previous = tmp;
		tmp = tmp->getParent();
		if (tmp->getRight() != nullptr && tmp->getRight() == previous) {
			parent = true;
		}
	}
	return root;
}
/* just like searchUp, we look up until we find correct node or stop node found
 * with only exception that we don't check node name*/
Node *SyntaxTree::searchUp(int stop, int nodes)
{
	Node *tmp = current;
	while (!(stop & tmp->getType())) {
		if (nodes & tmp->getType()) {
			return tmp;
		}

		if (tmp == root)
			return root;

		tmp = tmp->getParent();
	}

	if (nodes & tmp->getType()) {
		return tmp;
	}
	return root;
}
Node *SyntaxTree::getLeft()
{
	if (current != nullptr)
		return current->getLeft();
	return root;
}
Node *SyntaxTree::getRight()
{
	if (current != nullptr)
		return current->getRight();
	return root;
}

void SyntaxTree::goLeft()
{
	if (current != nullptr && current->getLeft() != nullptr)
		current = current->getLeft();
}

void SyntaxTree::goRight()
{
	if (current != nullptr && current->getRight() != nullptr)
		current = current->getRight();
}
void SyntaxTree::goUp()
{
	if (current != nullptr && current->getParent() != nullptr)
		current = current->getParent();
}
