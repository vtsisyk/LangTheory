#ifndef NODETYPE_H
#define NODETYPE_H

namespace NodeTypes {

enum Type {
	NONE = 0b0,
	UNKNOWN = 0b01,
	FUNCTION = 0b10,
	VARIABLE = 0b100,
	CONST = 0b1000,
	PARAMETER = 0b10000,
	CLASS = 0b100000,
	ROOT = 0b1000000,
};
}

namespace StopNodes {
enum StopNode {

	NONE = 0b0,
	RIGHT = 0b1,
	LEFT = 0b10,
};
}
typedef NodeTypes::Type NodeType;
typedef StopNodes::StopNode StopNode;

#endif /* ifndef NODETYPE_H */
