#ifndef NODE_H
#define NODE_H

#include <string>
#include <iostream>
#include <exception>
#include "../Lexer/defs.h"
#include "../Lexer/lexer.h"
#include "nodetype.h"

class Node;

union type_t{
	double double_type;
	int const10_type;
	bool boolean_type;
	Node *node_type;
};

class Node {
    private:
	NodeType type; /* It's used to understand which type node is. Like
			  'fucntion' or 'class' */
	Node *left;
	/* we should also store link to class or link to copy of it, not in
	 * separate node, but in *right* node */
	Node *right;
	Node *parent;


	/* Bit array of every possible information about node
	 *
	 * For example:
	 * bit  0        1       2            3       4 ...
	 *      +--------+-------+------------+-------+-----+
	 *      | double |integer| assignable | const | ... |
	 *      +--------+-------+------------+-------+-----+
	 *
	 * 4 bytes should be enough to store all possible flags
	 * */

	int info;

	/* every node usually have some name. For empty nodes i'll leave it empty */
	lexem_t lex;


	/* i have double, boolean and class types. */
	type_t value;

    public:
	Node();
	Node(NodeType t);

	lexem_t getLexem() { return lex; }
	std::string getName() { return lex.lexem; }
	void setLexem(lexem_t lexem) { lex = lexem; }
	void addInfo(int token);
	int getInfo() { return info; }
	void rmInfo(InfoType token);

	bool isset(int token);
	NodeType getType() { return type; }
	void setType(NodeType t) { type = t; }
	bool setLeft(Node *node);
	bool setRight(Node *node);
	bool setParent(Node *node);


	void rmBelow();
	void setValue(type_t val);
	type_t getValue();
	Node *copyNode();

	~Node();
	/*
	 * Ok, as we are wrtining interpeteur we *must* deep-copy some subtrees
	 *
	 * consider example 
	 *         class x
	 *      /          \
	 *  class y         *
	 *         \       /
	 *          *     a
	 *         /     /
	 *        x a   b
	 *       /
	 *      x b
	 * which in Java code looks like this:
	 *
	 *   class x{
	 *		int a,b;
	 *   }
	 *   class y{
	 *      x a;
	 *      x b;
	 *   }
	 *
	 *   what if we want assign some value to a.a and b.a?
	 *
	 *   If we would store only links to structure of a class(like we do in 
	 *   diagram analyzer), then we would not be able to do this. We have multiple
	 *   variables with same type, but only one storage
	 *
	 *   So, we have to deep-copy whole subtree
	 * */
	Node *copySubtree();


	void zres(){info = 0;}
	Node *getLeft() { return left; }
	Node *getRight() { return right; }
	Node *getParent() { return parent; }
};

#endif /* ifndef NODE_H*/
