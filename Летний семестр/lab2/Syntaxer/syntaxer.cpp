#include "syntaxer.h"
#include <iomanip>
#include <iostream>

Syntaxer::Syntaxer() {flint = false;}
Syntaxer::Syntaxer(const std::string &filename) {
	lexer.openFile(filename); 

	flint = false;;
}
/* class -> id -> class_block */
bool Syntaxer::sntx_class() {
	lex = lexer.next();

	/* class */
	if (lex.type != Tokens::KEYWORD_CLASS) {
		lexemMismatch(lex, Tokens::KEYWORD_CLASS);
		return false;
	}

	/* id */
	lex = lexer.next();
	if (lex.type != Tokens::IDENTIFIER) {
		lexemMismatch(lex, Tokens::IDENTIFIER);
		return false;
	}

	/* we search this class name until we find root */
	Node *tmp = tree.searchUp(lex.lexem, NodeType::ROOT, NodeType::CLASS);
	if (tree.getRoot() != tmp) {
		/* root with CLASS type and same name already exists */
		treeError(lex,
			  "class '" + lex.lexem + "' was already defined at: " +
			      std::to_string(lex.position.line) + "," +
			      std::to_string(lex.position.offset) + "\n",
			  lexer.getCurLine());
		return false;
	} else {
		/* attach new node and set it as current */
		Node *current = new Node(NodeType::CLASS);
		current->addInfo(InfoType::TYPE_CLASS);
		current->setLexem(lex);
		tree.getCurrent()->setLeft(current);
		tree.goLeft();

		/*           previous
		 *          /        \
		 *	  lex(current)
		 */
	}

	if (!sntx_class_block()) return false;
	return true;
}

/* { -> (function_declare | data | class )* -> }*/
bool Syntaxer::sntx_class_block() {
	Node *save = tree.getCurrent();
	lex = lexer.next();

	/* '}' */
	if (lex.type != Tokens::SEPARATOR_LBRACE) {
		lexemMismatch(lex, Tokens::SEPARATOR_LBRACE);
		return false;
	}

	Node *right = new Node();
	tree.getCurrent()->setRight(right);

	tree.goRight();
	/* 			root
	 *         /    \
	 *	    class
	 *	   /	 \
	 *	 	   current
	 */

	while (true) {
		position_t pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type == Tokens::IDENTIFIER) {
			/* SomeClass b,c,d*/
			lexer.setPos(pos);
			if (!sntx_data()) return false;
		} else if (lex.type == Tokens::KEYWORD_CLASS) {
			/* class SomeClass{}*/
			lexer.setPos(pos);
			if (!sntx_class()) return false;
		}

		else if (lex.type == Tokens::SEPARATOR_LBRACE) {
			lexer.setPos(pos);
			if (!sntx_class_block()) return false;
		} else if (lex.type == Tokens::KEYWORD_BOOLEAN ||
			   lex.type == Tokens::KEYWORD_DOUBLE) {
			lex = lexer.next();

			/* at this moment, can be both function or variable */
			if (lex.type == Tokens::IDENTIFIER) {
				lex = lexer.next();
				if (lex.type == Tokens::SEPARATOR_LPAR) {
					lexer.setPos(pos);
					if (!sntx_function_declare()) {
						return false;
					}
				} else if (lex.type ==
					       Tokens::OPERATOR_MATH_ASSIGN ||
					   lex.type ==
					       Tokens::SEPARATOR_SEMICOLON ||
					   lex.type ==
					       Tokens::SEPARATOR_COLON) {
					lexer.setPos(pos);
					if (!sntx_data()) return false;
				} else {
					lexemMismatch(lex, Tokens::SEPARATOR);
					return false;
				}
			} else {
				lexemMismatch(lex, Tokens::IDENTIFIER);
				return false;
			}
		} else {
			lexer.setPos(pos);
			break;
		}
	}

	lex = lexer.next();
	if (lex.type != Tokens::SEPARATOR_RBRACE) {
		lexemMismatch(lex, Tokens::SEPARATOR_RBRACE);
		return false;
	}

	tree.setCurrent(save);
	return true;
}

bool Syntaxer::sntx_data() {
	position_t pos = lexer.getPos();
	lex = lexer.next();

	InfoType type;
	Node *typenode;

	/* we should get type of variable for tree node here */
	if (lex.type == Tokens::KEYWORD_DOUBLE) {
		type = InfoType::TYPE_DOUBLE;
	} else if (lex.type == Tokens::KEYWORD_BOOLEAN) {
		type = InfoType::TYPE_BOOLEAN;
	} else {
		lexer.setPos(pos);
		if (!sntx_variable(NodeType::CLASS)) {
			return false;
		}

		if (tree.getRoot() == tree.found) {
			treeError(lex,
				  "class'" + lex.lexem +
				      "'expected, but was not found at: " +
				      std::to_string(lex.position.line) + "," +
				      std::to_string(lex.position.offset) +
				      "\n",
				  lexer.getCurLine());

			return false;
		} else {
			typenode = tree.found->getRight()->copySubtree();
			type = InfoType::TYPE_CLASS;
		}
	}

	while (1) {
		pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::IDENTIFIER) {
			lexemMismatch(lex, Tokens::IDENTIFIER);
			return false;
		}

		Node *tmp =
		    tree.searchUp(lex.lexem, NodeType::ROOT,
				  NodeType::VARIABLE | NodeType::PARAMETER);

		if (tree.getRoot() != tmp) {
			treeError(
			    lex,
			    "variable '" + lex.lexem +
				"' was already defined at: " +
				std::to_string(tmp->getLexem().position.line) +
				"," + std::to_string(
					  tmp->getLexem().position.offset) +
				"\n",
			    lexer.getCurLine());
			return false;
		} else {
			Node *current = new Node(NodeType::VARIABLE);
			current->addInfo(type);
			current->addInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			current->setLexem(lex);
			if (type == InfoType::TYPE_CLASS)
				current->setRight(typenode);

			tree.getCurrent()->setLeft(current);
			tree.goLeft();

			/* 			root
			 *        /     \
			 *	 current
			 */
		}

		lex = lexer.next();

		if (lex.type == Tokens::OPERATOR_MATH_ASSIGN) {
			lexer.setPos(pos);
			Node localres;
			if (!sntx_expression(&localres)) return false;
			Node *current = tree.getCurrent();
			/*
			if (current->isset((InfoType::ACCESS_MODIFIER_ASSIGNABLE) &&
						current->isset(localres.getInfo() &
					       InfoType::TYPE)) ||
			    (current->isset(InfoType::TYPE_DOUBLE) &&
			     localres.isset(InfoType::TYPE_CONST10))) {
			} else {
				treeError(
				    lex,
				    "Cannot assign '" + localres.getName() +
					"' to '" + current->getName() +
					"' at: " +
					std::to_string(lex.position.line) +
					"," +
					std::to_string(lex.position.offset) +
					"\n",
				    lexer.getCurLine());
				return false;
			}
		*/
		}
		if (lex.type == Tokens::SEPARATOR_COLON) {
		} else if (lex.type == Tokens::SEPARATOR_SEMICOLON) {
			//lexer.next();
			return true;
		} else {
			lexemMismatch(lex, Tokens::SEPARATOR_SEMICOLON);
			return false;
		}
	}

	return true;
}
/* double | boolean -> id -> ( -> sntx_arguments_declare -> ) -> sntx_block */
bool Syntaxer::sntx_function_declare() {
	lex = lexer.next();

	InfoType type;

	if (lex.type == Tokens::KEYWORD_DOUBLE) {
		type = InfoType::TYPE_DOUBLE;
	} else if (lex.type == Tokens::KEYWORD_BOOLEAN) {
		type = InfoType::TYPE_BOOLEAN;
	} else {
		lexemMismatch(lex, Tokens::KEYWORD);
		return false;
	}

	lex = lexer.next();

	if (lex.type != Tokens::IDENTIFIER) {
		lexemMismatch(lex, Tokens::IDENTIFIER);
		return false;
	}


	bool begin = false;
	if(lex.lexem == "main"){
		begin = true;
		flint = true;
	}

	/* we want to search up until we */
	Node *tmp =
	    tree.searchUp(lex.lexem, NodeType::CLASS, NodeType::FUNCTION);
	if (tree.getRoot() != tmp) {
		treeError(lex,
			  "function '" + lex.lexem +
			      "' was already defined at: " +
			      std::to_string(lex.position.line) + "," +
			      std::to_string(lex.position.offset) + "\n",
			  lexer.getCurLine());
		return false;
	} else {
		Node *current = new Node(NodeType::FUNCTION);
		current->setLexem(lex);
		current->addInfo(type);

		tree.getCurrent()->setLeft(current);

		tree.goLeft();
		/* 			we were here
		 *         /        	\
		 *	    current
		 *	   /	 \
		 */
	}

	Node *save = tree.getCurrent();
	lex = lexer.next();
	if (lex.type != Tokens::SEPARATOR_LPAR) {
		lexemMismatch(lex, Tokens::SEPARATOR_LPAR);
		return false;
	}

	if (!sntx_arguments_declare()) {
		return false;
	}

	lex = lexer.next();
	if (lex.type != Tokens::SEPARATOR_RPAR) {
		lexemMismatch(lex, Tokens::SEPARATOR_RPAR);
		return false;
	}

	if(flint == true && begin == false)
		flint = false;;
	
	if (!sntx_block()) return false;
		
	if(flint == false)
		flint = true;

	tree.setCurrent(save);
	return true;
}

bool Syntaxer::sntx_block() {
	Node *save = tree.getCurrent();
	lex = lexer.next();
	if (lex.type != Tokens::SEPARATOR_LBRACE) {
		lexemMismatch(lex, Tokens::SEPARATOR_LBRACE);
		return false;
	}

	
	Node *left = new Node();
	tree.getCurrent()->setLeft(left);
	left->setParent(tree.getCurrent());
	tree.goLeft();
	
	Node *right = new Node();
	tree.getCurrent()->setRight(right);
	right->setParent(tree.getCurrent());
	tree.goRight();
	/* 			root
	 *         /    \
	 *	    block
	 *	   /	 \
	 *	 	   current
	 */

	while (1) {
		position_t pos = lexer.getPos();
		lex = lexer.next();
		lexer.setPos(pos);
		if (lex.type == Tokens::KEYWORD_CLASS) {

			if (!sntx_class()) {
				return false;
			}
		}

		else if (lex.type == Tokens::KEYWORD_DOUBLE ||
			 lex.type == Tokens::KEYWORD_BOOLEAN) {
			
			if (!sntx_data()) {
				return false;
			}
		} else if (lex.type == Tokens::KEYWORD_RETURN ||
			   lex.type == Tokens::KEYWORD_IF ||
			   lex.type == Tokens::CONST10 ||
			   lex.type == Tokens::OPERATOR_MATH_PLUS ||
			   lex.type == Tokens::OPERATOR_MATH_MINUS ||
			   lex.type == Tokens::SEPARATOR_SEMICOLON ||
			   lex.type == Tokens::SEPARATOR_LPAR ||
			   lex.type == Tokens::SEPARATOR_LBRACE) {
			if (!sntx_operator()) {
				return false;
			}
		} else if (lex.type == Tokens::IDENTIFIER) {
			if (!sntx_variable(NodeType::FUNCTION |
					   NodeType::FUNCTION |
					   NodeType::CONST))
				return false;

			lex = lexer.next();

			if (lex.type == Tokens::IDENTIFIER) {
				lexer.setPos(pos);
				if (!sntx_data()) return false;
			} else {
				lexer.setPos(pos);
				if (!sntx_operator()) return false;
			}
		} else {
			lexer.setPos(pos);
			break;
		}
	}

	lex = lexer.next();
	if (lex.type != Tokens::SEPARATOR_RBRACE) {
		lexemMismatch(lex, Tokens::SEPARATOR_RBRACE);
		return false;
	}



	tree.setCurrent(save);

	/* Interpreter */

	//tree.getCurrent()->setLeft(left->getLeft());
	
	left->~Node();
	right->rmBelow();

	
	/* Interpreter */
	return true;
}

bool Syntaxer::sntx_operator() {
	position_t pos = lexer.getPos();
	lex = lexer.next();
	if (lex.type == Tokens::KEYWORD_RETURN) {
		lexer.setPos(pos);
		if (!sntx_return()) {
			return false;
		}
	} else if (lex.type == Tokens::KEYWORD_IF) {
		lexer.setPos(pos);
		if (!sntx_condition()) {
			return false;
		}
	} else if (lex.type == Tokens::SEPARATOR_LBRACE) {
		lexer.setPos(pos);
		if (!sntx_block()) {
			return false;
		}
	} else if (lex.type == Tokens::SEPARATOR_SEMICOLON) {
		return true;
	} else {
		lexer.setPos(pos);
		Node localres;
		if (!sntx_expression(&localres)) {
			return false;
		}

		lex = lexer.next();
		if(lex.type != Tokens::SEPARATOR_SEMICOLON){
			lexemMismatch(lex, Tokens::SEPARATOR_SEMICOLON);	
			return false;
		}
	}
	return true;
}

bool Syntaxer::sntx_condition() {


	/* Interpreter */
	bool localflint = flint;
	/* Interpreter */
	lex = lexer.next();
	position_t pos;

	if (lex.type != Tokens::KEYWORD_IF) {
		lexemMismatch(lex, Tokens::KEYWORD_IF);
		return false;
	}

	lex = lexer.next();
	if (lex.type != Tokens::SEPARATOR_LPAR) {
		lexemMismatch(lex, Tokens::SEPARATOR_LPAR);
		return false;
	}

	Node localres;
	if (!sntx_expression(&localres)) {
		return false;
	}

	/* Interpreter */
	if(flint && localres.getValue().boolean_type)
		flint = true;
	else
		flint = false;
	/* Interpreter */

	if (!localres.isset(InfoType::TYPE_BOOLEAN)) {
		treeError(
		    lex,
		    "Expected boolean, but '" + lex.lexem +
			"' was found at: " + std::to_string(lex.position.line) +
			"," + std::to_string(lex.position.offset) + "\n",
		    lexer.getCurLine());
		return false;
	}

	lex = lexer.next();
	if (lex.type != Tokens::SEPARATOR_RPAR) {
		lexemMismatch(lex, Tokens::SEPARATOR_RPAR);
		return false;
	}

	if (!sntx_operator()) {
		return false;
	}

	/* Interpreter */
	if (localflint)
		flint = !flint;
	/* Interpreter */

	pos = lexer.getPos();
	lex = lexer.next();
	if (lex.type == Tokens::KEYWORD_ELSE) {
		if (!sntx_operator()) {
			return false;
		}
	} else {
		lexer.setPos(pos);
	}

	/* Interpreter */
	flint = localflint;
	/* Interpreter */
	return true;
}
bool Syntaxer::sntx_return() {
	lex = lexer.next();

	if (lex.type != Tokens::KEYWORD_RETURN) {
		lexemMismatch(lex, Tokens::KEYWORD_RETURN);
		return false;
	}

	Node localres;
	if (!sntx_expression(&localres)) {
		return false;
	}
	Node *parentFunction =
	    tree.searchUp(NodeType::CLASS, NodeType::FUNCTION);


		if (parentFunction->isset(InfoType::TYPE_CLASS) &&
			    localres.isset(InfoType::TYPE_CLASS)) {
				if (parentFunction->getRight() == localres.getRight()) {
					/* good, do  something */
					/* скопипастить класс */
					if(flint){
						parentFunction->getRight()->rmBelow();
						parentFunction->setRight(localres.getRight()->copySubtree());
					}

				} else {
					treeError(
					    lex,
					    "Wrong return type of'" ,
					    lexer.getCurLine());
					return false;
				}

			/* Interpreter */
			} else if (parentFunction->isset(InfoType::TYPE_DOUBLE) && 
					   localres.isset(InfoType::TYPE_CONST10)) {
				if(flint){
					type_t val = localres.getValue();
					val.double_type = val.const10_type;
					parentFunction->setValue(val);
				}
			} else if (parentFunction->isset(InfoType::TYPE_DOUBLE) &&
				   localres.isset(InfoType::TYPE_DOUBLE)) {

				parentFunction->setValue(localres.getValue());
			} else if (parentFunction->isset(InfoType::TYPE_BOOLEAN) &&
				   localres.isset(InfoType::TYPE_BOOLEAN)) {
				parentFunction->setValue(localres.getValue());
	} else {
		treeError(lex,
			  "type of parent function and return type doesn't "
			  "match at: " +
			      std::to_string(lex.position.line) + "," +
			      std::to_string(lex.position.offset) + "\n",
			  lexer.getCurLine());
		return false;
	}

	if (lex.type != Tokens::SEPARATOR_SEMICOLON) {
		lexemMismatch(lex, Tokens::SEPARATOR_SEMICOLON);
		return false;
	}


	return true;
}

bool Syntaxer::sntx_arguments_declare() {
	position_t pos;

	Node *right = new Node();
	tree.getCurrent()->setRight(right);

	tree.goRight();

	InfoType type;
	while (1) {
		Node *typenode;
		pos = lexer.getPos();
		lex = lexer.next();

		if (lex.type == Tokens::KEYWORD_DOUBLE) {
			type = InfoType::TYPE_DOUBLE;
		} else if (lex.type == Tokens::KEYWORD_BOOLEAN) {
			type = InfoType::TYPE_BOOLEAN;
		} else if (lex.type == Tokens::IDENTIFIER) {
			lexer.setPos(pos);
			if (!sntx_variable(NodeType::CLASS)) return false;
			typenode = tree.found;
			type = InfoType::TYPE_CLASS;
		} else if(lex.type == Tokens::SEPARATOR_RPAR){
			lexer.setPos(pos);
			return true;
		} else {
			treeError(
			    lex,
			    "Wrong type of parameter",
			    lexer.getCurLine());
			return false;
		}
		lex = lexer.next();
		if (lex.type != Tokens::IDENTIFIER) {
			lexemMismatch(lex, Tokens::IDENTIFIER);
			return false;
		}

		Node *tmp = tree.searchUp(lex.lexem, NodeType::FUNCTION,
					  NodeType::PARAMETER);
		if (tree.getRoot() != tmp) {
			treeError(
			    lex,
			    "Arguments with such name was already declared: '" +
				lex.lexem + "'  at: " +
				std::to_string(lex.position.line) + "," +
				std::to_string(lex.position.offset) + "\n",
			    lexer.getCurLine());

			/* TODO: Error */
			return false;
		}

		Node *param = new Node(NodeType::PARAMETER);
		param->setLexem(lex);
		param->addInfo(type | InfoType::ACCESS_MODIFIER_ASSIGNABLE);
		if (type == InfoType::TYPE_CLASS) {
			param->setRight(typenode);
		}

		tree.getCurrent()->setLeft(param);
		tree.goLeft();
		/*
		 *		fucntion
		 *				\
		 *  			we were here
		 *        		 /        	\
		 *	    	 current
		 *	  		/      \
		 */

		pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::SEPARATOR_COLON) {
			lexer.setPos(pos);
			break;
		}
	}
	Node *left = new Node();
	tree.getCurrent()->setLeft(left);

	tree.goLeft();

	return true;
}
bool Syntaxer::sntx_arguments_call() {
	position_t pos;

	Node *current = tree.found;

	current = current->getRight();
	while (1) {
	current = current->getLeft();
		pos = lexer.getPos();
		lex = lexer.next();

		if (!(lex.type == Tokens::IDENTIFIER || lex.type == Tokens::CONST10)) {
			lexemMismatch(lex, Tokens::IDENTIFIER);
			return false;
		} 
		lexer.setPos(pos);

		Node localres;
		
		if(!sntx_expression(&localres))
			return false;

		pos = lexer.getPos();
		
		Node tmpnode;
		if(flint){
			type_t val;

			val.node_type = current;
			tmpnode.setValue(val);
			tmpnode.setType(NodeType::VARIABLE);
		}
			if (current->isset(InfoType::TYPE_CLASS) &&
			    localres.isset(InfoType::TYPE_CLASS)) {
				if (current->getRight() == localres.getRight()) {
					/* good, do  something */
					/* скопипастить класс */
					if(flint){
						current->getRight()->rmBelow();
						current->setRight(localres.getRight()->copySubtree());
					}
				} else {
					treeError(
					    lexer.current(pos),
					    "Class variable '" +
						current->getLexem().lexem +
						"' has different type than declared argument '" +
						localres.getLexem().lexem + "'",
					    lexer.getCurLine());
					return false;
				}


			/* Interpreter */
			} else if (current->isset(InfoType::TYPE_DOUBLE) && 
					   localres.isset(InfoType::TYPE_CONST10)) {
				if(flint){
					type_t val;
					val.double_type = localres.getValue().const10_type;
					tmpnode.setValue(val);
				}
			} else if (current->isset(InfoType::TYPE_DOUBLE) &&
				   localres.isset(InfoType::TYPE_DOUBLE)) {
				if(flint){
					tmpnode.setValue(localres.getValue());
				}
			} else if (current->isset(InfoType::TYPE_BOOLEAN) &&
				   localres.isset(InfoType::TYPE_BOOLEAN)) {
				if(flint){
					tmpnode.setValue(localres.getValue());
				}
			} else if(current->getType() == NodeType::UNKNOWN ){
				treeError(lexer.current(pos), "Number of arguments doesnt' match", lexer.getCurLine());
						return false;
			}else{
				treeError(lexer.current(pos), "Expression '" + localres.getLexem().lexem +
						"' has different type than '" + current->getLexem().lexem + "'\n", lexer.getCurLine());
						return false;
			}

		pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::SEPARATOR_COLON) {
			lexer.setPos(pos);
			break;
		}
	}

	tree.setCurrent(current);
	return true;
}
bool Syntaxer::sntx_function_call(Node *result) {

	if (!sntx_variable(NodeType::FUNCTION)) {
		return false;
	}


	/* Interpreter */

	Node *current = tree.getCurrent();
	Node *save = tree.found;



	Node *function;

	if(flint){

		function = tree.found->copyNode();
		function->setRight(tree.found->getRight()->copySubtree());
		function->setLeft(tree.found->getLeft());
		tree.found->setLeft(function);
		function->setParent(tree.found);
		function->getLeft()->setParent(function);

		tree.found = function;
		tree.setCurrent(function);
		/* Interpreter */
	}

	if (tree.found->getType() != NodeType::FUNCTION) {
		treeError(lex,
			  "'" + tree.found->getName() +
			      "' is not a function. At" +
			      std::to_string(lex.position.line) + "," +
			      std::to_string(lex.position.offset) + "\n",
			  lexer.getCurLine());
		return false;
	}

	/* should be found in above function */
	lex = lexer.next();

	if (lex.type != Tokens::SEPARATOR_LPAR) {
		lexemMismatch(lex, Tokens::SEPARATOR_LPAR);
		return false;
	}

	if (!sntx_arguments_call()) {
		return false;
	}

	lex = lexer.next();

	if (lex.type != Tokens::SEPARATOR_RPAR) {
		lexemMismatch(lex, Tokens::SEPARATOR_RPAR);
		return false;
	}

	

	if(flint){
	/* Interpreter */
	position_t pos = lexer.getPos();
	lexer.setPos(tree.getCurrent()->getLexem().position);
	/* skip '(' */
	lexer.next();
	

	if(!sntx_block()){
		return false;
	}

	result->setValue(function->getValue());


		lexer.setPos(pos);


		tree.found = save;

		tree.setCurrent(current);

		tree.found->setLeft(function->getLeft());
		function->getLeft()->setParent(tree.found);

		function->getRight()->rmBelow();
		function->~Node();
		/* Interpreter */

	}

	/*
	lex = lexer.next();
	if (lex.type != Tokens::SEPARATOR_SEMICOLON) {
		lexemMismatch(lex, Tokens::SEPARATOR_SEMICOLON);
		return false;
	}
	*/
	return true;
}
bool Syntaxer::sntx_variable(int looktype) {
	position_t pos;

	Node *current = tree.getCurrent();
	Node *tmp;
	while (1) {
		pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::IDENTIFIER) {
			lexemMismatch(lex, Tokens::IDENTIFIER);
			return false;
		}

		lex = lexer.next();

		lexer.setPos(pos);
		while (tree.getLeft() != nullptr) {
			tree.goLeft();
		}
		if (lex.type == Tokens::SEPARATOR_LPAR) {
			lex = lexer.next();
			tmp = tree.searchUp(lex.lexem, NodeType::CLASS,
					    NodeType::FUNCTION);
			break;
		} else if (lex.type == Tokens::SEPARATOR_LPAR) {
			lex = lexer.next();
			tmp = tree.searchUp(
			    lex.lexem, NodeType::ROOT,
			    NodeType::VARIABLE | NodeType::PARAMETER);
		} else {
			lex = lexer.next();
			if (looktype & NodeType::VARIABLE) {
				tmp = tree.searchUp(
				    lex.lexem, NodeType::ROOT,
				    NodeType::VARIABLE | NodeType::PARAMETER);
				if (tree.getRoot() == tmp) {
					treeError(lex,
						  "variable '" + lex.lexem +
						      "' was not declared\n",
						  lexer.getCurLine());
					return false;
				}
			} else if (looktype & NodeType::CLASS) {
				tmp = tree.searchUp(
				    lex.lexem, NodeType::ROOT,
				    NodeType::CLASS);
				if (tree.getRoot() == tmp) {
					treeError(lex,
						  "Class '" + lex.lexem +
						      "' was not declared\n",
						  lexer.getCurLine());
					return false;
				}
			}
			

			break;
		}

		tree.setCurrent(tmp);
		tree.goRight();
	}

	tree.found = tmp;
	tree.setCurrent(current);
	return true;
}

void Syntaxer::lexemMismatch(lexem_t lex, TokenType token) {
	std::cout << "\033[1;31mSyntax error\033[0m at " << lex.position.line
		  << ", " << lex.position.offset
		  << ": expected '" + lexer.getTypeName(token) + "', but got '"
		  << lex.lexem << "'(" + lex.typestr + ")"
		  << "\n";

	std::cout << lexer.getCurLine() << "\n";
	std::cout << std::setw(lex.position.offset + 1) << "^\n";
	std::cout << "\n";
}
void Syntaxer::run() {
	bool runned = sntx_class();
	std::cout << "Syntax check...";
	if (runned) {
		std::cout << " \033[1;32mOK\033[0m\n";
	} else {
		std::cout << " \033[1;31mFailed\033[0m\n";
	}
}

void Syntaxer::treeError(lexem_t lex, std::string str, std::string line) {
	std::cout << "\033[1;31mSyntax error(\033[0m\033[1;33mtree "
		     "build\033[0m\033[1;31m)\033[0m at "
		  << lex.position.line << "," << lex.position.offset << ": ";
	std::cout << str;
	//		std::cout << line;
	std::cout << "\n";
}

bool Syntaxer::sntx_expression(Node *result) {
	/*
	 * a = b = c = d + 5
	 * <------that way---
	 *
	 * for this reason we have to use stack;
	 * */

	
	/* Interpreter */
	std::stack<Node > stk;
	/* Interpreter */

	if (!sntx_exp2(result)) {
		return false;
	}


	if(flint){
		if (result->getType() == NodeType::VARIABLE) {
			if(result->isset(InfoType::ACCESS_MODIFIER_ASSIGNABLE)){
				stk.push(*result);
			}
		}
	}

	Node localres;

	while (1) {
		position_t pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::OPERATOR_MATH_ASSIGN) {
			lexer.setPos(pos);
			break;
		}

		pos = lexer.getPos();
		if (!sntx_exp2(&localres)) {
			return false;
		}

		if (result->getType() == NodeType::VARIABLE) {
			if (result->isset(InfoType::TYPE_CLASS) &&
			    localres.isset(InfoType::TYPE_CLASS)) {
				if (result->getRight() == localres.getRight()) {
					/* good, do  something */
					/* скопипастить класс */
					if(flint){
						result->getRight()->rmBelow();
						result->setRight(localres.getRight()->copySubtree());
					}

				} else {
					treeError(
					    lexer.current(pos),
					    "Class variable '" +
						result->getLexem().lexem +
						"' cannot be assigned by '" +
						localres.getLexem().lexem + "'",
					    lexer.getCurLine());
					return false;
				}

			/* Interpreter */
			} else if (result->isset(InfoType::TYPE_DOUBLE) && 
					   localres.isset(InfoType::TYPE_CONST10)) {
			} else if (result->isset(InfoType::TYPE_DOUBLE) &&
				   localres.isset(InfoType::TYPE_DOUBLE)) {
			} else if (result->isset(InfoType::TYPE_BOOLEAN) &&
				   localres.isset(InfoType::TYPE_BOOLEAN)) {
			} else {
				treeError(lexer.current(pos), "Can't assign '" + localres.getLexem().lexem +
						"' to '" + result->getLexem().lexem + "'\n", lexer.getCurLine());
						return false;
			}
		/* Interpreter */

			/* result <- (result = localres) */
			if (!localres.isset(InfoType::ACCESS_MODIFIER_ASSIGNABLE)){
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else {

				if(flint)
					stk.push(localres);
			}
		} else {
			treeError(localres.getLexem(),
				  "variable '" + result->getLexem().lexem +
				      "' cannot be assigned by '" +
				      localres.getLexem().lexem + "'",
				  lexer.getCurLine());
			return false;
		}
	}


	if(flint){
#ifdef DEBUG
 	std::cout << "-- stack begin -- \n";
#endif
	while(!stk.empty()){
		std::cout << stk.top().getLexem().lexem << "\n";
		if (stk.top().isset(InfoType::TYPE_CLASS) &&
			    localres.isset(InfoType::TYPE_CLASS)) {
					/* good, do  something */
					/* скопипастить класс */

#ifdef DEBUG
 	std::cout <<  stk.top().getLexem().lexem << " = " << localres.getLexem().lexem <<" \n";
#endif
			/* Interpreter */
			} else if (stk.top().isset(InfoType::TYPE_DOUBLE) && 
					   localres.isset(InfoType::TYPE_CONST10)) {
				type_t val = localres.getValue();
				val.double_type = val.const10_type;
				stk.top().setValue(val);
#ifdef DEBUG
 	std::cout <<  stk.top().getLexem().lexem << " = "<< val.double_type<<  " \n";
#endif
			} else if (stk.top().isset(InfoType::TYPE_DOUBLE) &&
				   localres.isset(InfoType::TYPE_DOUBLE)) {
				type_t val = localres.getValue();
				stk.top().setValue(val);

#ifdef DEBUG
 	std::cout <<  stk.top().getLexem().lexem << " = "<< val.double_type<<  " \n";
#endif
			} else if (stk.top().isset(InfoType::TYPE_BOOLEAN) &&
				   localres.isset(InfoType::TYPE_BOOLEAN)) {
				type_t val = localres.getValue();
				stk.top().setValue(val);
#ifdef DEBUG
 	std::cout <<  stk.top().getLexem().lexem << " = "<< val.boolean_type <<" \n";
#endif
			}

		stk.pop();
	}

#ifdef DEBUG
 	std::cout << "-- stack end -- \n";
#endif
	}
	return true;
}

bool Syntaxer::sntx_exp2(Node *result) {
	if (!sntx_exp3(result)) {
		return false;
	}

	Node localres;
	while (1) {
		position_t pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::OPERATOR_LOGIC_AND) {
			lexer.setPos(pos);
			return true;
		}

		pos = lexer.getPos();
		if (!sntx_exp3(&localres)) {
			return false;
		}


		if(result->isset(InfoType::TYPE_BOOLEAN) && localres.isset(InfoType::TYPE_BOOLEAN)){
			if(flint){
				type_t val;
				val.boolean_type = result->getValue().boolean_type &&
								   localres.getValue().boolean_type;
				result->setValue(val);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			}
		} else {
			treeError(lexer.current(pos),
				  "'" + result->getLexem().lexem +
				      "' types don't match.",
				  lexer.getCurLine());
			return false;
		}
	}
	return true;
}
bool Syntaxer::sntx_exp3(Node *result) {
	if (!sntx_exp4(result)) {
		return false;
	}
	Node localres;

	while (1) {
		position_t pos = lexer.getPos();
		lexem_t operation;
		lex = lexer.next();
		if (lex.type != Tokens::OPERATOR_LOGIC_EQ &&
		    lex.type != Tokens::OPERATOR_LOGIC_NEQ) {
			lexer.setPos(pos);
			return true;
		}

		operation = lex;
		pos = lexer.getPos();
		if (!sntx_exp4(&localres)) {
			return false;
		}

		if(result->isset(InfoType::TYPE_DOUBLE) && localres.isset(InfoType::TYPE_DOUBLE)){
			if(operation.type == Tokens::OPERATOR_LOGIC_EQ){
				result->setType(NodeType::CONST);
				if(flint){
					type_t val;
					val.boolean_type = result->getValue().double_type == localres.getValue().double_type;
					result->setValue(val);
				}
					result->rmInfo(InfoType::TYPE_DOUBLE);
					result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
					result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_NEQ){
					result->setType(NodeType::CONST);
				if(flint){
					type_t val;
					val.boolean_type = result->getValue().double_type != localres.getValue().double_type;
					result->setValue(val);
				}
					result->rmInfo(InfoType::TYPE_DOUBLE);
					result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
					result->addInfo(InfoType::TYPE_BOOLEAN);

			}
		} else if(result->isset(InfoType::TYPE_DOUBLE) && localres.isset(InfoType::TYPE_CONST10)) {
			if(operation.type == Tokens::OPERATOR_LOGIC_EQ){
				
				result->setType(NodeType::CONST);
				if(flint){

				type_t val;
				val.boolean_type = result->getValue().double_type == localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_NEQ){
				result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type != localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			}
		} else if (result->isset(InfoType::TYPE_CONST10) && localres.isset(InfoType::TYPE_DOUBLE)) {
			if(operation.type == Tokens::OPERATOR_LOGIC_EQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type == localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_NEQ){
				result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type != localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			}
		
		} else if(result->isset(InfoType::TYPE_BOOLEAN) && localres.isset(InfoType::TYPE_BOOLEAN)){
			if(operation.type == Tokens::OPERATOR_LOGIC_EQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().boolean_type == localres.getValue().boolean_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_NEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().boolean_type!= localres.getValue().boolean_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			}
		} else {
			treeError(lexer.current(pos),
				  "'" + result->getLexem().lexem +
				      "' types don't match.",
				  lexer.getCurLine());
			return false;
		}		
	}
	return true;
}
bool Syntaxer::sntx_exp4(Node *result) {
	if (!sntx_exp5(result)) {
		return false;
	}

	Node localres;
	while (1) {
		position_t pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::OPERATOR_LOGIC_GE &&
		    lex.type != Tokens::OPERATOR_LOGIC_BEQ &&
		    lex.type != Tokens::OPERATOR_LOGIC_LT &&
		    lex.type != Tokens::OPERATOR_LOGIC_LEQ) {
			lexer.setPos(pos);
			return true;
		}

		lexem_t operation = lex;
		pos = lexer.getPos();
		if (!sntx_exp5(&localres)) {
			return false;
		}
		if(result->isset(InfoType::TYPE_DOUBLE) && localres.isset(InfoType::TYPE_DOUBLE)){
			if(operation.type == Tokens::OPERATOR_LOGIC_GE){
				result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type > localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_BEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type >= localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LT){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type < localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			}else if(operation.type == Tokens::OPERATOR_LOGIC_LEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type <= localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			}

		} else if(result->isset(InfoType::TYPE_CONST10) && localres.isset(InfoType::TYPE_CONST10)) {
			if(operation.type == Tokens::OPERATOR_LOGIC_GE){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type > localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_BEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type >= localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LT){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type < localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type <= localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			}
		} else if(result->isset(InfoType::TYPE_DOUBLE) && localres.isset(InfoType::TYPE_CONST10)) {
			if(operation.type == Tokens::OPERATOR_LOGIC_GE){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type > localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_BEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type >= localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LT){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type < localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().double_type <= localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_DOUBLE);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			}
		} else if (result->isset(InfoType::TYPE_CONST10) && localres.isset(InfoType::TYPE_DOUBLE)) {
			if(operation.type == Tokens::OPERATOR_LOGIC_GE){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type > localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_BEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type >= localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LT){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type < localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().const10_type <= localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::TYPE_CONST10);
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			}
		} else if(result->isset(InfoType::TYPE_BOOLEAN) && localres.isset(InfoType::TYPE_BOOLEAN)){
			if(operation.type == Tokens::OPERATOR_LOGIC_GE){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().boolean_type > localres.getValue().boolean_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->addInfo(InfoType::TYPE_BOOLEAN);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_BEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().boolean_type >= localres.getValue().boolean_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LT){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().boolean_type < localres.getValue().boolean_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_LOGIC_LEQ){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.boolean_type = result->getValue().boolean_type <= localres.getValue().boolean_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			}
		}  else {
			treeError(lexer.current(pos),
				  "'" + result->getLexem().lexem +
				      "' types don't match.",
				  lexer.getCurLine());
			return false;
		}
	}
	return true;
}

bool Syntaxer::sntx_exp5(Node *result) {
	if (!sntx_exp6(result)) {
		return false;
	}

	Node localres;
	while (1) {
		position_t pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::OPERATOR_MATH_MINUS &&
		    lex.type != Tokens::OPERATOR_MATH_PLUS) {
			lexer.setPos(pos);
			return true;
		}

		lexem_t operation = lex;
		pos = lexer.getPos();
		if (!sntx_exp6(&localres)) {
			return false;
		}

		if(result->isset(InfoType::TYPE_DOUBLE) && localres.isset(InfoType::TYPE_DOUBLE)){
			if(operation.type == Tokens::OPERATOR_MATH_PLUS){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val ;
				val.double_type = result->getValue().double_type + localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_MATH_MINUS){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().double_type - localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			}
		} else if(result->isset(InfoType::TYPE_DOUBLE) && localres.isset(InfoType::TYPE_CONST10)){
			if(operation.type == Tokens::OPERATOR_MATH_PLUS){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().double_type + localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_MATH_MINUS){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().double_type - localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			}
		} else if(result->isset(InfoType::TYPE_CONST10) && localres.isset(InfoType::TYPE_DOUBLE)){
			if(operation.type == Tokens::OPERATOR_MATH_PLUS){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().const10_type + localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->rmInfo(InfoType::TYPE_CONST10);
				result->addInfo(InfoType::TYPE_DOUBLE);
			} else if(operation.type == Tokens::OPERATOR_MATH_MINUS){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().const10_type - localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->rmInfo(InfoType::TYPE_CONST10);
				result->addInfo(InfoType::TYPE_DOUBLE);
			}
		} else if(result->isset(InfoType::TYPE_CONST10) && localres.isset(InfoType::TYPE_CONST10)){
			if(operation.type == Tokens::OPERATOR_MATH_PLUS){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.const10_type = result->getValue().const10_type + localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_MATH_MINUS){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.const10_type = result->getValue().const10_type - localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			}
		}  
		
		else {
			treeError(lexer.current(pos),
				  "'" + result->getLexem().lexem +
				      "' types don't match.",
				  lexer.getCurLine());
			return false;
		}
	}
	return true;
}
bool Syntaxer::sntx_exp6(Node *result) {
	if (!sntx_exp7(result)) {
		return false;
	}

	Node localres;
	while (1) {
		position_t pos = lexer.getPos();
		lex = lexer.next();
		if (lex.type != Tokens::OPERATOR_MATH_MODULO &&
		    lex.type != Tokens::OPERATOR_MATH_DIVISION &&
		    lex.type != Tokens::OPERATOR_MATH_MULTIPLY) {
			lexer.setPos(pos);
			return true;
		}

		lexem_t operation = lex;
		pos = lexer.getPos();

		if (!sntx_exp7(&localres)) {
			return false;
		}

		if(result->isset(InfoType::TYPE_DOUBLE) && localres.isset(InfoType::TYPE_DOUBLE)){
			if(operation.type == Tokens::OPERATOR_MATH_MODULO){
				treeError(lexer.current(pos),
				  "'" + result->getLexem().lexem +
				      "' types don't match.",
				  lexer.getCurLine());
				return false;

				/* err */
			} else if(operation.type == Tokens::OPERATOR_MATH_DIVISION){
					result->setType(NodeType::CONST);
				if(flint){
					type_t val;
					val.double_type = result->getValue().double_type / localres.getValue().double_type;
					result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_MATH_MULTIPLY){
					result->setType(NodeType::CONST);

				if(flint){
					type_t val;
					val.double_type = result->getValue().double_type / localres.getValue().double_type;
					result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			}
		} else if(result->isset(InfoType::TYPE_DOUBLE) && localres.isset(InfoType::TYPE_CONST10)){
			if(operation.type == Tokens::OPERATOR_MATH_MODULO){
				treeError(lexer.current(pos),
				  "'" + result->getLexem().lexem +
				      "' types don't match.",
				  lexer.getCurLine());
				return false;

				/* err */
				
			} else if(operation.type == Tokens::OPERATOR_MATH_DIVISION){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().double_type / localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_MATH_MULTIPLY){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().double_type * localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			}

		} else if(result->isset(InfoType::TYPE_CONST10) && localres.isset(InfoType::TYPE_DOUBLE)){
			if(operation.type == Tokens::OPERATOR_MATH_MODULO){
				treeError(lexer.current(pos),
				  "'" + result->getLexem().lexem +
				      "' types don't match.",
				  lexer.getCurLine());
				return false;
			} else if(operation.type == Tokens::OPERATOR_MATH_DIVISION){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().const10_type / localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->rmInfo(InfoType::TYPE_CONST10);
				result->addInfo(InfoType::TYPE_DOUBLE);
			} else if(operation.type == Tokens::OPERATOR_MATH_MULTIPLY){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = result->getValue().const10_type / localres.getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
				result->rmInfo(InfoType::TYPE_CONST10);
				result->addInfo(InfoType::TYPE_DOUBLE);
			}

		} else if(result->isset(InfoType::TYPE_CONST10) && localres.isset(InfoType::TYPE_CONST10)){
			if(operation.type == Tokens::OPERATOR_MATH_MODULO){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.const10_type = result->getValue().const10_type / localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			
			} else if(operation.type == Tokens::OPERATOR_MATH_DIVISION){
			
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.const10_type = result->getValue().const10_type / localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			} else if(operation.type == Tokens::OPERATOR_MATH_MULTIPLY){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.const10_type = result->getValue().const10_type / localres.getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
			}

		} else {
			treeError(lexer.current(pos),
				  "'" + result->getLexem().lexem +
				      "' types don't match.",
				  lexer.getCurLine());
			return false;
		}
	}
	return true;
}
bool Syntaxer::sntx_exp7(Node *result) {
	position_t pos = lexer.getPos();
	lex = lexer.next();
	lexem_t operation = lex;
	if (lex.type != Tokens::OPERATOR_MATH_PLUS &&
	    lex.type != Tokens::OPERATOR_MATH_MINUS) {
		lexer.setPos(pos);

		if (!sntx_exp8(result)) {
			return false;
		}
		return true;
	}

	pos = lexer.getPos();
	if (!sntx_exp8(result)) {
		return false;
	}

	if(operation.type == Tokens::OPERATOR_MATH_MINUS){
		if(result->isset(InfoType::TYPE_CONST10)){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.const10_type = (-1) * result->getValue().const10_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
		} else if(result->isset(InfoType::TYPE_DOUBLE)){
					result->setType(NodeType::CONST);
				if(flint){
				type_t val;
				val.double_type = (-1) * result->getValue().double_type;
				result->setValue(val);
				}
				result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
		}
	} else {
		treeError(
		    result->getLexem(),
		    "'" + result->getLexem().lexem + "' types don't match.",
		    lexer.getCurLine());
		return false;
	}

	return true;
}
bool Syntaxer::sntx_exp8(Node *result) {
	position_t pos = lexer.getPos();
	lex = lexer.next();
	if (lex.type == Tokens::CONST10) {
		result->setLexem(lex);
		result->zres();
		result->setType(NodeType::CONST);
		result->addInfo(InfoType::TYPE_CONST10);
		result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);

				if(flint){
		/* Interpreter */
		type_t val;
		val.const10_type = atoi(lex.lexem.c_str());
		result->setValue(val);
		/* Interpreter */
				}
		return true;
	} else if (lex.type == Tokens::SEPARATOR_LPAR) {
		Node localnode;
		if (!sntx_expression(&localnode)) {
			return false;
		}
		result->addInfo(localnode.getInfo());

		result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
		lex = lexer.next();
		if (lex.type != Tokens::SEPARATOR_RPAR) {
			lexemMismatch(lex, Tokens::SEPARATOR_RPAR);
			return false;
		}
	}

	else if (lex.type == Tokens::IDENTIFIER) {
		lexer.setPos(pos);
		if (!sntx_variable(NodeType::CONST | NodeType::VARIABLE |
				   NodeType::FUNCTION | NodeType::PARAMETER))
			return false;
		if (tree.found->isset(InfoType::TYPE_CLASS)) {
			result->setRight(tree.found->getRight());
		}

				if(flint){
		/* Interpreter */
		type_t val;
		val.node_type = tree.found;

		result->setValue(val);
		/* Interpreter */
				}
		result->setLexem(tree.found->getLexem());
		result->zres();
		result->addInfo(tree.found->getInfo());
		
		position_t localpos = lexer.getPos();
		lex = lexer.next();
		
		if (lex.type == Tokens::SEPARATOR_LPAR) {
			
			lexer.setPos(pos);
			if (!sntx_function_call(result)) {
				return false;
			}

			result->setType(NodeType::FUNCTION);
			result->rmInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE);
		} else {
			lexer.setPos(localpos);
			result->setType(NodeType::VARIABLE);
			result->addInfo(InfoType::ACCESS_MODIFIER_ASSIGNABLE |
					tree.found->getInfo());
		}
	}
	return true;
}
