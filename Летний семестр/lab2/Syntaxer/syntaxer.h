/* this class does syntax check and helps building syntax tree */
#ifndef SYNTAXER_H
#define SYNTAXER_H

#include <string>
#include "../Lexer/lexer.h"
#include "../SyntaxTree/syntaxtree.h"

#include <stack>

#define DEBUG

class Syntaxer {
    private:
    /* lexem parser */
	Lexer lexer;

	/* current lexem */
	lexem_t lex;
	
	/* diagrams */
	bool sntx_class();
	bool sntx_class_block();

	bool sntx_data();
	bool sntx_function_declare();
	bool sntx_block();
	bool sntx_operator();
	bool sntx_condition();
	bool sntx_return();

	bool sntx_arguments_declare();
	bool sntx_arguments_call();
	bool sntx_function_call(Node *);
	bool sntx_variable(int);
	bool sntx_variable_type();


	bool sntx_expression(Node *);
	bool sntx_exp2(Node *);
	bool sntx_exp3(Node *);
	bool sntx_exp4(Node *);
	bool sntx_exp5(Node *);
	bool sntx_exp6(Node *);
	bool sntx_exp7(Node *);
	bool sntx_exp8(Node *);


	/* syntaxtree */
	SyntaxTree tree;
	
	/* errors */
	void lexemMismatch(lexem_t lex, TokenType);
	void treeError(lexem_t lex, std::string str, std::string line);


	/* interpretation flag */
	bool flint;


    public:
	Syntaxer();

	void run();
	Syntaxer(const std::string &filename);
};
#endif /* ifndef SYNTAXER_H */
