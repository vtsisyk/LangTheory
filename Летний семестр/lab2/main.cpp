#include <iostream>
#include "Syntaxer/syntaxer.h"

int main(int argc, char *argv[])
{
	printf("== Lexer Parser, Syntax Diagrams and Tree Builder ==\n");
	Syntaxer syn("test.java");

	syn.run();

	return 0;
}
